import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './login/login.module';
import { HomeModule } from './home/home.module';
import { NavigationModule } from './navigation/navigation.module';
import { InvoiceModule } from './invoices/invoices.module';
import { ReportsModule } from './reports/reports.module';
import { ScheduleModule } from './schedule/schedule.module';
import { SharedModule } from './shared/shared.module';
import { CustomersModule } from './customers/customers.module';
import { ProductsModule } from './products/products.module';
import { PurchasesModule } from './purchases/purchases.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SpinnerOverlayComponent } from './spinner-overlay/spinner-overlay.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { AccountsModule } from './accounts/accounts.module';
import { AccountsRoutingModule } from './accounts/accounts-routing.module';
import { BranchesModule } from './branches/branches.module';
import { BranchesRoutingModule } from './branches/branches-routing.module';
import { RolesRoutingModule } from './roles/roles-routing.module';
import { RolesModule } from './roles/roles.module';
import { AuthenticationInterceptor } from './shared/interceptors/authentication.interceptor';
import { ChangelogRoutingModule } from './changelog/changelog-routing.module';
import { ChangelogModule } from './changelog/changelog.module';
import { CreateCustomerDialogComponent } from './customers/create-customer-dialog.component';
import { DeliveryStatusDialogComponent } from './invoices/delivery-status-dialog.component';
import { ConfirmActionDialogComponent } from './shared/confirm-action-dialog.component';
import { PublicModule } from './public/public.module';
import { QueueRoutingModule } from './queue/queue-routing.module';
import { QueueModule } from './queue/queue.module';
import { PaymentDetailsDialogComponent } from './invoices/payment-details-dialog.component';
import { AuditsComponent } from './audits/audits.component';
import { AuditsModule } from './audits/audits.module';
import { AuditsRoutingModule } from './audits/audits-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    SpinnerOverlayComponent
  ],
  imports: [
    BrowserModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    OverlayModule,
    HomeModule,
    LoginModule,
    NavigationModule,
    InvoiceModule,
    ReportsModule,
    ScheduleModule,
    SharedModule,
    CustomersModule,
    ProductsModule,
    PurchasesModule,
    AccountsModule,
    AccountsRoutingModule,
    BranchesModule,
    BranchesRoutingModule,
    RolesModule,
    RolesRoutingModule,
    ChangelogModule,
    ChangelogRoutingModule,
    QueueModule,
    QueueRoutingModule,
    AuditsModule,
    AuditsRoutingModule,
    AppRoutingModule,
    PublicModule,
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthenticationInterceptor,
    multi: true
  },
    Title
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    SpinnerOverlayComponent,
    CreateCustomerDialogComponent,
    DeliveryStatusDialogComponent,
    PaymentDetailsDialogComponent,
    ConfirmActionDialogComponent,
  ]
})
export class AppModule { }
