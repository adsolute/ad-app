import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicDocumentComponent } from './public-document.component';
import { SharedModule } from '@progress/kendo-angular-dropdowns';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';



@NgModule({
  declarations: [PublicDocumentComponent],
  imports: [
    CommonModule,
    PdfJsViewerModule
  ],
  exports: [
    PublicDocumentComponent
  ]
})
export class PublicModule { }
