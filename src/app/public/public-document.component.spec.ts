import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicDocumentComponent } from './public-document.component';

describe('PublicDocumentComponent', () => {
  let component: PublicDocumentComponent;
  let fixture: ComponentFixture<PublicDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
