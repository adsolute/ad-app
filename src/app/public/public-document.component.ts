import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from '../shared/services/http.service';
import { Invoice } from '../invoices/invoice';
import { environment } from 'src/environments/environment';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-public',
  templateUrl: './public-document.component.html',
  styleUrls: ['./public-document.component.css']
})
export class PublicDocumentComponent implements OnInit {

  pdfUrl: string;

  constructor(private reportHttpService: HttpService<any>) {
  }

  ngOnInit() {
    let publicUrl = window.location.href;
    this.pdfUrl = publicUrl.split("public")[1].split("?")[0];
    this.pdfUrl = environment.backendApiUrl + this.pdfUrl +"?doc-type=PDF";
    console.log("D", this.pdfUrl)
    this.generatePdf();
  }

  @ViewChild('externalPdfViewer', {static: false}) public externalPdfViewer;
  generatePdf() {
    this.reportHttpService.getWithResponseType(
      this.pdfUrl,
    'blob')
      .subscribe({
        next: (httpResponse: HttpResponse<any>) => {
          console.log(httpResponse)
          this.externalPdfViewer.pdfSrc = httpResponse;
          this.externalPdfViewer.refresh();
        }
      });
  }
}
