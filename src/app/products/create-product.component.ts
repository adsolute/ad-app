import { Component, OnInit } from '@angular/core';
import { HttpService } from '../shared/services/http.service';
import { Product } from './product';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpResponse } from '@angular/common/http';
import { CanUpdateChecker } from '../interfaces/can-update-checker';
import { PermissionCheckUtils } from '../shared/permission-check-utils';
import { Resource, Action } from '../roles/role';
import { CanDeleteChecker } from '../interfaces/can-delete-checker';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmActionDialogComponent } from '../shared/confirm-action-dialog.component';

@Component({
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit, CanUpdateChecker, CanDeleteChecker {

  product: Product = new Product();
  viewMode: boolean = false;
  headerMode = "Add";
  canUpdate: boolean = false;
  canDelete: boolean = false;

  constructor(
    private httpService: HttpService<Product>,
    private toastrService: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private matDialog: MatDialog) {
      if("view" == activatedRoute.snapshot.url[1].path) {
        this.viewMode = true;
        let id: string = this.activatedRoute.snapshot.paramMap.get("id");
        this.httpService.get(environment.backendApiUrl+"/products/product?id="+id+"&branch-id="+localStorage.getItem("activeBranchId")).subscribe({
          next : (httpResponse: HttpResponse<Product>) => {
            this.product = httpResponse.body;
          },
          error: error => {
            this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" });
            this.router.navigate(["product"]);
          }
        });
      }
    }

  ngOnInit() {
    this.canUpdate = this.checkCanUpdate();
    this.canDelete = this.checkCanDelete();
  }

  saveProduct(){
    if(this.product.name && this.product.description && this.product.productCode && this.product.productType && this.product.productCategory) {
      if(this.product.id == null) {
        this.createNewProduct();
      } else{
        this.updateProduct();
      }
    } else {
      this.toastrService.warning("Please complete all fields", "Incomplete Fields", { positionClass: "toast-top-center" });
    }
  }

  createNewProduct() {
    this.httpService.post(environment.backendApiUrl+"/products?branch-id="+localStorage.getItem("activeBranchId"), this.product).subscribe({
      next: () => {
        this.toastrService.success("Product Added", "Success", { positionClass: "toast-top-center" });
        this.router.navigate(["products"]);
      },
      error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }

  updateProduct() {
    this.httpService.put(environment.backendApiUrl+"/products?branch-id="+localStorage.getItem("activeBranchId"), this.product).subscribe({
      next: () => {
        this.toastrService.success("Product Added", "Success", { positionClass: "toast-top-center" });
        this.router.navigate(["products"]);
      },
      error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }

  openConfirmDeleteDialog() {
    const dialogRef = this.matDialog.open(ConfirmActionDialogComponent, {
      width: '500px', data: {id: this.product.id, action: "Delete"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.httpService.delete(environment.backendApiUrl+"/products/"+this.product.id+"?branch-id="+localStorage.getItem("activeBranchId"))
          .subscribe({
            next: () => {
              this.toastrService.success("Product Deleted", "Success", { positionClass: "toast-top-center" });
              this.router.navigate(["products"]);
            },
            error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          });
      }
    });
  }

  checkCanUpdate():boolean {
    return PermissionCheckUtils.checkPermissions(Resource.PRODUCTS, Action.UPDATE);
  }
  
  checkCanDelete():boolean {
    return PermissionCheckUtils.checkPermissions(Resource.PRODUCTS, Action.DELETE);
  }
}
