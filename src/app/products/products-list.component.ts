import { Component, OnInit } from '@angular/core';
import { PaginatedList } from '../interfaces/paginated-list';
import { Product } from './product'
import { HttpService } from '../shared/services/http.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { PagedResultSet } from '../shared/paged-result-set';
import { QueryParameters } from '../shared/query-parameters';
import { PagedList } from '../shared/paged-list';
import { Resource } from '../roles/role';
import { environment } from 'src/environments/environment';
import { Header, Filter, DateRangeFilter } from '../shared/paginated-table.component';

@Component({
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent extends PagedList implements OnInit, PaginatedList {

  productResultSet: PagedResultSet<Product>  = new PagedResultSet;
  queryParameters: QueryParameters = new QueryParameters;
  hasCreatePermission: boolean = false;

  constructor(private httpService: HttpService<Product>, private toastrService: ToastrService, private router: Router) {
    super();
   }

  getHeaders(): Header[]{
    return [
      {display: "name", actual: "name", sortable: true},
      {display: "productCode", actual: "productCode", sortable: true},
      {display: "price", actual: "price", sortable: true},
    ];
  }

  filters: Filter[] = [
    { name: "Name", key: "name"},
    { name: "Product Code", key: "number"},
  ];
  
  dateRanges: DateRangeFilter[] = [
  ];

  ngOnInit() {
    this.queryParameters.page = 1;
    this.queryParameters.limit = 50;
    this.sendQueryParameters();
  }

  getPages = (): number[] => {
    return super.getPages(this.productResultSet);
  }
  
  sendQueryParameters() {
    this.httpService.postQuery<QueryParameters, PagedResultSet<Product>>(environment.backendApiUrl+"/products/query?branch-id=" + localStorage.getItem("activeBranchId"), this.queryParameters).subscribe({
      next: (httpResponse: HttpResponse<PagedResultSet<Product>>) => this.productResultSet = httpResponse.body,
      error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  };

  goToAddProductLink(): void {
    this.router.navigate(["products/create"]);
  }

  hideCreateButton = () => super.hideCreateButton(Resource.PRODUCTS);
}
