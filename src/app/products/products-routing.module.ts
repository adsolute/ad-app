import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProductsListComponent } from './products-list.component';
import { CreateProductComponent } from './create-product.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: "products", component: ProductsListComponent },
      { path: "products/create", component: CreateProductComponent },
      { path: "products/view/:id", component: CreateProductComponent }
    ])
  ]
})
export class ProductsRoutingModule { }
