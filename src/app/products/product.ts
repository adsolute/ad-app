export class Product {
    id: string;
    name: string;
    price: number;
    description: string;
    productCode: string;
    productType: ProductType;
    productCategory: ProductCategory;
    numberOfPax: number;
}

export enum ProductType {
    MAIN="MAIN",
    ADD_ON="ADD_ON"
}

export enum ProductCategory {
    UNLIMITED="UNLIMITED",
    LIMITED="LIMITED"
}