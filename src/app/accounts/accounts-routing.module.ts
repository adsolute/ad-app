import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AccountsListComponent } from './accounts-list.component';
import { CreateAccountComponent } from './create-account.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: "accounts", component: AccountsListComponent },
      { path: "accounts/create", component: CreateAccountComponent },
      { path: "accounts/view/:username", component: CreateAccountComponent },
    ]),
  ]
})
export class AccountsRoutingModule { }
