import { Component, OnInit } from '@angular/core';
import { PaginatedList } from '../interfaces/paginated-list';
import { Router } from '@angular/router';
import { PagedList } from '../shared/paged-list';
import { HttpService } from '../shared/services/http.service';
import { UserAccount } from '../shared/user-account';
import { QueryParameters } from '../shared/query-parameters';
import { PagedResultSet } from '../shared/paged-result-set';
import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Resource } from '../roles/role';
import { environment } from 'src/environments/environment';
import { Header, Filter, DateRangeFilter } from '../shared/paginated-table.component';

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.css']
})
export class AccountsListComponent extends PagedList implements OnInit, PaginatedList {

  userAccountPagedResultSet: PagedResultSet<any> = new PagedResultSet;
  queryParameters: QueryParameters = new QueryParameters;


  accounts: any;

  constructor(
    private httpService: HttpService<UserAccount>,
    private toastrService: ToastrService,
    private router: Router) {
      super();
      this.queryParameters.page = 1;
      this.queryParameters.limit = 50;
      this.sendQueryParameters();
     }

  getHeaders(): Header[] {
    return [
      {display: "username", actual: "username", sortable: true},
      {display: "email", actual: "email", sortable: false},
      {display: "phone", actual: "phone", sortable: false},
    ];
  }

  filters: Filter[] = [
    { name: "Username", key: "username"},
  ];
  
  dateRanges: DateRangeFilter[] = [
  ];

  ngOnInit() {
  }

  getPages = () => super.getPages(this.userAccountPagedResultSet);
  sendQueryParameters() {
    this.httpService.postQuery<QueryParameters, PagedResultSet<UserAccount>>(environment.iamApiUrl+"/user-accounts/query?branch-id=" + localStorage.getItem("activeBranchId"), this.queryParameters)
      .subscribe({
        next: (httpResponse: HttpResponse<PagedResultSet<UserAccount>>) => this.userAccountPagedResultSet = httpResponse.body,
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }
  
  goToCreateAccountLink() {
    this.router.navigate(["accounts/create"]);
  }

  hideCreateButton = () => super.hideCreateButton(Resource.ACCOUNTS);
}
