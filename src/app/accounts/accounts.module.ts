import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountsListComponent } from './accounts-list.component';
import { SharedModule } from '../shared/shared.module';
import { CreateAccountComponent } from './create-account.component';



@NgModule({
  declarations: [AccountsListComponent, CreateAccountComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class AccountsModule { }
