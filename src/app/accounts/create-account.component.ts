import { Component, OnInit } from '@angular/core';
import { HttpService } from '../shared/services/http.service';
import { Role, Resource, Action } from '../roles/role';
import { Branch } from '../branches/branch';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { User } from '../shared/user';
import { GrantedRole } from '../roles/granted-role';
import { UserAccount } from '../shared/user-account';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { PermissionCheckUtils } from '../shared/permission-check-utils';
import { CanDeleteChecker } from '../interfaces/can-delete-checker';
import { CanUpdateChecker } from '../interfaces/can-update-checker';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmActionDialogComponent } from '../shared/confirm-action-dialog.component';

@Component({
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit, CanUpdateChecker, CanDeleteChecker  {

  userAccount:UserAccount = new UserAccount;
  user:any = {};
  createdUser: User;
  branches: Branch[];
  roles: Role[];
  selectedBranch: Branch;
  associatedBranchesAndRoles = [];
  viewMode: boolean = false;
  headerMode: string = "Add";
  canUpdate: boolean = false;
  canDelete: boolean = false;
  activeBranchId: string = localStorage.getItem("activeBranchId");
  isAdmin: boolean = '*'==localStorage.getItem('activeBranchId');

  constructor(
    private branchHttpService: HttpService<Branch>,
    private roleHttpService: HttpService<Role>,
    private userHttpService: HttpService<User>,
    private toastrService: ToastrService,
    private userAccountHttpService: HttpService<UserAccount>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private matDialog: MatDialog) {
      if("view" == activatedRoute.snapshot.url[1].path) {
        this.getRolesOnce();
        this.viewMode = true;
        let username: string = this.activatedRoute.snapshot.paramMap.get("username");
        this.getUserByAccountId(username);
      }
    }

  ngOnInit() {
    this.canUpdate = this.checkCanUpdate();
    this.canDelete = this.checkCanDelete();
  }

  getUserByAccountId(username: string) {
    this.userHttpService.get(environment.iamApiUrl+"/users/"+username+"?branch-id="+this.activeBranchId).subscribe({
      next : (httpResponse: HttpResponse<User>) => {
        this.createdUser = httpResponse.body;
        this.user = this.createdUser;
        this.getExistingUserAccount(this.createdUser.accountId);
      },
      error: error => {
        this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" });
        this.router.navigate(["accounts"]);
      }
    });
  }

  getExistingUserAccount(accountId: string) {
    this.userAccountHttpService.get(environment.iamApiUrl+"/user-accounts/account?account-id="+accountId+"&branch-id="+this.activeBranchId).subscribe({
      next : (httpResponse: HttpResponse<UserAccount>) => {
        if (httpResponse.body != null) {
          this.userAccount = httpResponse.body;
        }
        this.populateAssociatedBranchesAndRoles(this.userAccount.grantedRoles);
      },
      error: error => {
        this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" });
        this.router.navigate(["accounts"]);
      }
    });
  }

  populateAssociatedBranchesAndRoles(grantedRoles: GrantedRole[]) {
    this.branchHttpService.get(environment.backendApiUrl+"/branches")
      .subscribe({
        next: (httpResponse: HttpResponse<Branch[]>) => {
          this.branches = httpResponse.body;
          grantedRoles.map( grantedRole => {
            let branch: Branch = this.branches.find(branch => branch.branchId == grantedRole.onAccountId);
            console.log("Adding this branch", branch);
            let role: Role = this.roles.find(role => role.roleId == grantedRole.roleId);
            console.log("Adding this role", role);
            if(branch) {
              this.associatedBranchesAndRoles.push({
                accountId: branch.branchId,
                name: branch.name,
                role: role
              });
            }
          });
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  branchSelected(selectedBranch: Branch) {
    this.selectedBranch = selectedBranch;
  }

  addSelectedBranch() {
    this.getRolesOnce();
    if(this.selectedBranch != null){
      let notYetAdded: boolean = this.associatedBranchesAndRoles
        .filter(associatedBranchesAndRole => associatedBranchesAndRole.name == this.selectedBranch.name).length == 0;
      if(notYetAdded) {
        this.associatedBranchesAndRoles.push({
          name: this.selectedBranch.name,
          accountId: this.selectedBranch.branchId
        });
      } else {
        this.toastrService.warning("Branch Already Added", "Invalid Action", { positionClass: "toast-top-center" });
      }
    }
  }

  getRolesOnce(): void {
    if(this.roles == null) {
      this.roleHttpService.get(environment.iamApiUrl+"/roles")
      .subscribe({
        next: (httpResponse: HttpResponse<Role[]>) => {
          this.roles = httpResponse.body;
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
    }
  }

  selectRole(role: Role, associatedBranchAndRole): void{
    associatedBranchAndRole.role =  role;
  }
  
  createUser() {
    if(this.user.password == this.user.confirmPassword) {
      let createUser: User = new User();
      createUser.username = this.user.username;
      createUser.password = this.user.password;
      let passwordIsStrong: boolean = this.determinePasswordStrength(createUser.password);
      if(passwordIsStrong) {
        this.userHttpService.post(environment.iamApiUrl+"/users?branch-id="+this.activeBranchId, createUser).subscribe({
        next:(httpResponse: HttpResponse<User>) => {
          this.createdUser = httpResponse.body;
          this.getBranches();
        },
        error:(error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
      }
    } else {
      this.toastrService.error("Password and Confirm Password didn't match", "An Error Occured", { positionClass: "toast-top-center" });
    }
  }

  determinePasswordStrength(password: string): boolean {
    const hasSpecial = /[$-/:-?{-~!"^_@`\[\]]/g.test(password);
    const hasLower = /[a-z]+/.test(password);
    const hasUpper = /[A-Z]+/.test(password);
    const hasNumber = /[0-9]+/.test(password);
    let passwordWarningMessage: string = "";
    if(!hasSpecial) {
      passwordWarningMessage += "&#8226 Must contain atleast 1 special character<br>";
    }
    if(!hasLower) {
      passwordWarningMessage += "&#8226 Must contain atleast 1 lowercase character<br>";
    }
    if(!hasUpper) {
      passwordWarningMessage += "&#8226 Must contain atleast 1 uppercase character<br>";
    }
    if(!hasNumber) {
      passwordWarningMessage += "&#8226 Must contain atleast 1 numeric character<br>";
    }
    if(passwordWarningMessage.length > 0) {
      this.toastrService.warning(passwordWarningMessage, "Password Not Strong Enough", {timeOut: 5000, positionClass:  "toast-top-center", enableHtml: true });
    }
    return hasSpecial && hasLower && hasUpper && hasNumber;
  }

  getBranches() {
    this.branchHttpService.get(environment.backendApiUrl+"/branches")
      .subscribe({
        next: (httpResponse: HttpResponse<Branch[]>) => this.branches = httpResponse.body,
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  saveUserAccount() {
    this.userAccount.grantedRoles = [];
    this.associatedBranchesAndRoles.map(branchAndRole => {
      console.log("Assoc", branchAndRole);
      if(!branchAndRole.role) {
        this.toastrService.warning("Please assign a role" , "Invalid Action", { positionClass: "toast-top-center" })
      }
      this.userAccount.grantedRoles.push({
        onAccountId: branchAndRole.accountId,
        onAccountName: branchAndRole.name,
        roleId: branchAndRole.role.roleId,
        roleName: branchAndRole.role.name
      })
    });
    console.log("saving", this.userAccount)
    if(this.userAccount.accountId == null) {
      this.userAccountHttpService.post(environment.iamApiUrl+"/user-accounts?branch-id="+this.activeBranchId, this.userAccount).subscribe({
        next:(httpResponse: HttpResponse<UserAccount>) => {
          this.userAccount = httpResponse.body
          this.updateUser()
        },
        error: (httpErrorResponse: HttpErrorResponse) => this.toastrService.error(httpErrorResponse.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
    } else {
      this.userAccountHttpService.put(environment.iamApiUrl+"/user-accounts?branch-id="+this.activeBranchId, this.userAccount).subscribe({
        next:(httpResponse: HttpResponse<UserAccount>) => {
          this.toastrService.success("User Account Saved", "Success", { positionClass: "toast-top-center" });
          this.router.navigate(["accounts"]);
        },
        error: (httpErrorResponse: HttpErrorResponse) => this.toastrService.error(httpErrorResponse.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
    }
  }

  updateUser() {
    this.createdUser.accountId = this.userAccount.accountId
    this.userHttpService.put(environment.iamApiUrl+"/users?branch-id="+this.activeBranchId, this.createdUser).subscribe({
      next: () => {
        this.toastrService.success("User Account Saved", "Success", { positionClass: "toast-top-center" });
        this.router.navigate(["accounts"]);
      },
      error: (httpErrorResponse: HttpErrorResponse) => this.toastrService.error(httpErrorResponse.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }

  checkCanUpdate():boolean {
    return PermissionCheckUtils.checkPermissions(Resource.ACCOUNTS, Action.UPDATE);
  }
  
  checkCanDelete():boolean {
    return PermissionCheckUtils.checkPermissions(Resource.ACCOUNTS, Action.DELETE);
  }

  openConfirmDeleteDialog() {
    const dialogRef = this.matDialog.open(ConfirmActionDialogComponent, {
      width: '500px', data: {id: this.userAccount.id, action: "Delete"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.userAccountHttpService.delete(environment.iamApiUrl+"/user-accounts/"+this.user.username+"?branch-id="+this.activeBranchId)
          .subscribe({
            next: () => {
              this.toastrService.success("Account Deleted", "Success", { positionClass: "toast-top-center" });
              this.router.navigate(["accounts"]);
            },
            error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          });
      }
    });
  }
}
