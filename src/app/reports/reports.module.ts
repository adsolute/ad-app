import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CountUpModule } from 'ngx-countup';
import { ChartsModule } from '@progress/kendo-angular-charts'

@NgModule({
  declarations: [ReportsComponent],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule,
    CountUpModule,
    ChartsModule
  ]
})
export class ReportsModule { }
