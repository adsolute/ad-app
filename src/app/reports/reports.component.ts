import { Component, OnInit } from '@angular/core';
import { DateRangeFilter } from '../shared/paginated-table.component';
import { TransactionType } from '../invoices/invoice';
import { HttpService } from '../shared/services/http.service';
import { Branch } from '../branches/branch';
import { environment } from 'src/environments/environment';
import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { QueryParameters } from '../shared/query-parameters';
import * as saveAs from "file-saver";
import * as moment from 'moment';

@Component({
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  invoiceSalesReport: InvoiceSalesReport;
  dateRangeFilter: DateRangeFilter = new DateRangeFilter();
  activeTransactionType: string = "ALL";
  transactionTypes: string[] = ["ALL", TransactionType.DELIVERY, TransactionType.DINE_IN];
  activeReportType: string = "Total Sales";
  reportTypes: string[] = ["Total Sales", "Daily Sales"];
  branches: Branch[];
  selectedBranchName: string = "ALL";
  branchNames: string[] = ["ALL"];
  isAdmin: boolean = false;
  branchIdForReport: string;
  hours: string[] = ["6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00"];
  ordersPlaced = [];

  constructor(
    private toastrService: ToastrService,
    private branchHttpService: HttpService<Branch>,
    private httpService: HttpService<any>) { }

  ngOnInit() {
    this.isAdmin = '*'==localStorage.getItem('activeBranchId');
    this.getBranches();
  }

  getBranches() {
    this.branchHttpService.get(environment.backendApiUrl+"/branches")
      .subscribe({
        next: (httpResponse: HttpResponse<Branch[]>) => {
          this.branches = httpResponse.body;
          httpResponse.body.map(branch => {
            this.branchNames.push(branch.name);
          });
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  selectBranch(name: string) {
    let branch = this.branches.find(branch => {
      return branch.name == name;
    })
    if(branch) {
      this.branchIdForReport = branch.branchId;
    }
  }

  generateReport() {
    let queryParameters: QueryParameters = this.buildQueryParameters();
    this.invoiceSalesReport = undefined;
    if(this.activeReportType == "Total Sales") {
      this.generateTotalSalesReport(queryParameters);
    } else if (this.activeReportType == "Daily Sales") {
      this.downloadDailySalesReport(queryParameters);
    }
  }

  private buildQueryParameters(): QueryParameters {
    let queryParameters = new QueryParameters();
    if(this.activeTransactionType != "ALL") {
      queryParameters.criteria["transactionType"] = this.activeTransactionType;
    }
    if(this.isAdmin && this.selectedBranchName != "ALL") {
      queryParameters.criteria["branchId"] = this.branchIdForReport;
    } else if(!this.isAdmin) {
      queryParameters.criteria["branchId"] = localStorage.getItem("activeBranchId");
    }
    if(this.dateRangeFilter.start && this.dateRangeFilter.end) {
      this.dateRangeFilter.start.setHours(0, 0, 0);
      this.dateRangeFilter.end.setHours(23, 59, 59);
      queryParameters.dateRanges = [{
        field: "deliveryDate",
        start: this.dateRangeFilter.start,
        end: this.dateRangeFilter.end
      }];
    } else {
      if(this.dateRangeFilter.start || this.dateRangeFilter.end) {
        this.toastrService.warning("Please Select Date Range", "Start and End Dates are required", { positionClass: "toast-top-center" })
        return;
      }
    }

    return queryParameters;
  }

  private generateTotalSalesReport(queryParameters: QueryParameters) {
    this.httpService.postQuery<QueryParameters, any>(environment.backendApiUrl+"/reports/invoice-sales/query?branch-id=" + localStorage.getItem("activeBranchId"), queryParameters)
      .subscribe({
        next: (httpResponse: HttpResponse<any>) => {
          this.invoiceSalesReport = httpResponse.body;
          this.ordersPlaced = [];
          this.hours.forEach(hour => {
            let totalInvoiceForThisColumn: TotalInvoiceByHour;
            this.invoiceSalesReport.totalInvoiceByHours.forEach(totalInvoiceByHour => {
              if(hour == totalInvoiceByHour.hour+":00" && totalInvoiceByHour.hour != "0"){
                totalInvoiceForThisColumn = totalInvoiceByHour;
              }
            })
            if(totalInvoiceForThisColumn){
              this.ordersPlaced.push(totalInvoiceForThisColumn.totalInvoice)
            } else {
              this.ordersPlaced.push(0);
            }
          })
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  private downloadDailySalesReport(queryParameters: QueryParameters) {
    this.httpService.post(environment.backendApiUrl+"/reports/document/daily-sales?branch-id=" + localStorage.getItem("activeBranchId"), queryParameters)
      .subscribe({
        next: (httpResponse: HttpResponse<any>) => {
          let docType: string;
          if (this.isAdmin) {
            docType="XLS"
          } else {
            docType="PDF"
          }
          this.httpService.getWithResponseType(
            httpResponse.headers.get("Location")+"?doc-type="+docType,
          'blob')
            .subscribe({
              next: (httpResponse: HttpResponse<any>) => {
                console.log(httpResponse)
                if (this.isAdmin) {
                  if(this.dateRangeFilter.start && this.dateRangeFilter.end) {
                    let startDate: string = moment(this.dateRangeFilter.start).format("MMM-DD-YYYY");
                    let endDate: string = moment(this.dateRangeFilter.end).format("MMM-DD-YYYY");
                    saveAs(httpResponse, this.selectedBranchName+` BRANCH_`+startDate+`_`+endDate+`_`+`DAILY SALES.xls`)
                  } else {
                    saveAs(httpResponse, this.selectedBranchName+` BRANCH_ALL DATES_`+`DAILY SALES.xls`)
                  }
                } else {
                  let branch = this.branches.find(branch => {
                    return branch.branchId == localStorage.getItem("activeBranchId");
                  })
                  if(this.dateRangeFilter.start && this.dateRangeFilter.end) {
                    let startDate: string = moment(this.dateRangeFilter.start).format("MMM-DD-YYYY");
                    let endDate: string = moment(this.dateRangeFilter.end).format("MMM-DD-YYYY");
                    saveAs(httpResponse, branch.name+` BRANCH_`+startDate+`_`+endDate+`_`+`DAILY SALES.pdf`)
                  } else {
                    saveAs(httpResponse, branch.name+` BRANCH_ALL DATES_`+`DAILY SALES.pdf`)
                  }
                }
              },
              error: error => this.toastrService.error(error.message, "An Error Occured", { positionClass: "toast-top-center" })
            });
        },
        error: error => this.toastrService.error(error.message, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }
}

export class InvoiceSalesReport {
  totalSales: number;
  totalOrders: number;
  totalInvoiceByHours: TotalInvoiceByHour[];
}

export class TotalInvoiceByHour {
  hour: string;
  totalInvoice: number;
}
