import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CustomersListComponent } from './customers-list.component';
import { CreateCustomerComponent } from './create-customer.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: "customers", component: CustomersListComponent },
      { path: "customers/create", component: CreateCustomerComponent },
      { path: "customers/view/:id", component: CreateCustomerComponent },
    ])
  ]
})
export class CustomersRoutingModule { }
