import { Component, OnInit } from '@angular/core';
import { Customer } from './customer';
import { ContactInformation } from "../shared/contact-information";
import { HttpService } from '../shared/services/http.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Address } from '../shared/address';
import { HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CanUpdateChecker } from '../interfaces/can-update-checker';
import { PermissionCheckUtils } from '../shared/permission-check-utils';
import { Resource, Action } from '../roles/role';
import { ConfirmActionDialogComponent } from '../shared/confirm-action-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerComponent implements OnInit, CanUpdateChecker {

  customer: Customer = new Customer;
  contactInformation: ContactInformation = new ContactInformation;
  address: Address = new Address;
  viewMode: boolean = false;
  headerMode: string = "Add";
  canUpdate: boolean = false;

  constructor(
    private httpService: HttpService<Customer>,
    private toastrService: ToastrService, 
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private matDialog: MatDialog) {
      if("view" == activatedRoute.snapshot.url[1].path) {
        this.viewMode = true;
        let id: string = this.activatedRoute.snapshot.paramMap.get("id");
        this.httpService.get(environment.backendApiUrl+"/customers/customer?id="+id+"&branch-id="+localStorage.getItem("activeBranchId")).subscribe({
          next : (httpResponse: HttpResponse<Customer>) => {
            this.address = httpResponse.body.address;
            this.contactInformation = httpResponse.body.contactInformation;
            this.customer = httpResponse.body;
          },
          error: error => {
            this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" });
            this.router.navigate(["customers"]);
          }
        });
      }
    }

  ngOnInit() {
    this.address.country = "Philippines";
    this.canUpdate = this.checkCanUpdate();
  }

  addCustomer(){
    this.customer.address = this.address;
    this.customer.contactInformation = this.contactInformation;
    this.httpService.post(environment.backendApiUrl+"/customers", this.customer).subscribe({
      next: () => {
        this.toastrService.success("Customer Added", "Success", { positionClass: "toast-top-center" });
        this.router.navigate(["customers"]);
      },
      error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }

  checkCanUpdate():boolean {
    return PermissionCheckUtils.checkPermissions(Resource.INVOICES, Action.UPDATE);
  }

  openConfirmDeleteDialog() {
    const dialogRef = this.matDialog.open(ConfirmActionDialogComponent, {
      width: '500px', data: {id: this.customer.id, action: "Delete"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.httpService.delete(environment.backendApiUrl+"/customers/"+this.customer.id+"?branch-id="+localStorage.getItem("activeBranchId"))
          .subscribe({
            next: () => {
              this.toastrService.success("Customer Deleted", "Success", { positionClass: "toast-top-center" });
              this.router.navigate(["customers"]);
            },
            error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          });
      }
    });
  }
}
