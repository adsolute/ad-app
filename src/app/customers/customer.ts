import { ContactInformation } from '../shared/contact-information';
import { Address } from '../shared/address';

export class Customer {
    id: string;
    name: string;
    contactInformation: ContactInformation;
    address: Address
}