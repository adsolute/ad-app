import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersListComponent } from './customers-list.component';
import { SharedModule } from '../shared/shared.module';
import { CreateCustomerComponent } from './create-customer.component';
import { CreateCustomerDialogComponent } from './create-customer-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';



@NgModule({
  declarations: [
    CustomersListComponent,
    CreateCustomerComponent,
    CreateCustomerDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CustomersRoutingModule,
    MatDialogModule,
  ]
})
export class CustomersModule { }
