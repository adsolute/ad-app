import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Customer } from './customer';
import { Address } from '../shared/address';
import { ContactInformation } from '../shared/contact-information';
import { HttpService } from '../shared/services/http.service';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-customer-dialog',
  templateUrl: './create-customer-dialog.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerDialogComponent implements OnInit {
  customer: Customer = new Customer();
  address: Address = new Address();
  contactInformation: ContactInformation = new ContactInformation();
  headerMode: string = "Add";
  viewMode: boolean = false;

  constructor(
    private dialogRef: MatDialogRef<CreateCustomerDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    private data: Customer,
    private httpService: HttpService<Customer>,
    private toastrService: ToastrService) {}

  ngOnInit() {
    this.address.country = "Philippines";
  }

  addCustomer(){
    this.customer.address = this.address;
    this.customer.contactInformation = this.contactInformation;
    this.httpService.post(environment.backendApiUrl+"/customers", this.customer).subscribe({
      next: () => {
        this.toastrService.success("Customer Added", "Success", { positionClass: "toast-top-center" });
        this.dialogRef.close({event:"closed", customer:this.customer});
      },
      error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }
}
