import { Component, OnInit } from '@angular/core';
import { PaginatedList } from '../interfaces/paginated-list';
import { HttpService } from '../shared/services/http.service';
import { Customer } from './customer';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { PagedList } from '../shared/paged-list';
import { PagedResultSet } from '../shared/paged-result-set';
import { QueryParameters } from '../shared/query-parameters';
import { Resource } from '../roles/role';
import { environment } from 'src/environments/environment';
import { Header, Filter, DateRangeFilter } from '../shared/paginated-table.component';

@Component({
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent extends PagedList implements OnInit, PaginatedList {
  
  customerPagedResultSet: PagedResultSet<Customer>  = new PagedResultSet;
  queryParameters: QueryParameters = new QueryParameters;

  constructor(
    private httpService: HttpService<Customer>,
    private toastrService: ToastrService,
    private router: Router) {
      super();
     }

  ngOnInit() {
    this.queryParameters.page = 1;
    this.queryParameters.limit = 50;
    this.sendQueryParameters();
  }


  getPages = () => super.getPages(this.customerPagedResultSet);
  sendQueryParameters = () => {
    this.httpService.postQuery<QueryParameters, PagedResultSet<Customer>>(environment.backendApiUrl+"/customers/query?branch-id=" + localStorage.getItem("activeBranchId"), this.queryParameters).subscribe({
      next: (httpResponse: HttpResponse<PagedResultSet<Customer>>) => this.customerPagedResultSet = httpResponse.body,
      error: (error) => this.toastrService.error(error.error, "An Error Occured", {positionClass: "toast-top-center"})
    });
  }

  getHeaders(): Header[] {
    return [
      {display: "name", actual: "name", sortable: true},
      {display: "emailAddress", actual: "emailAddress", sortable: true},
      {display: "phoneNumber", actual: "phoneNumber", sortable: true},
    ]
  }

  filters: Filter[] = [
    { name: "Name", key: "name"},
    { name: "Email", key: "contactInformation.emailAddress"},
    { name: "Phone", key: "contactInformation.phoneNumber"},
  ];
  
  dateRanges: DateRangeFilter[] = [
  ];


  goToAddCustomerLink() {
    this.router.navigate(["customers/create"]);
  }

  hideCreateButton = () => super.hideCreateButton(Resource.CUSTOMERS);
}
