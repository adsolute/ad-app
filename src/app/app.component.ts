import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpService } from './shared/services/http.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  isLoggedIn: boolean = JSON.parse(sessionStorage.getItem("isLoggedIn"));
  loggedInSubscription: Subscription;
  onPublicView: boolean = false;

  constructor(
    private httpService: HttpService<any>,
    private toastrService: ToastrService,
    private title: Title){}

  ngOnInit() {
    if(location.pathname.includes("public")) {
      this.onPublicView = true;
    }
    this.title.setTitle(environment.brandName);
    if(!this.onPublicView) {
      this.loggedInSubscription = this.httpService.stayLoggedInObervable
        .subscribe(stayLoggedIn => this.isLoggedIn = stayLoggedIn);
      this.httpService.get(environment.iamApiUrl+"/auth/check")
        .subscribe({
          error: (httpErrorResponse: HttpErrorResponse) => {
            this.isLoggedIn = false;
            if(httpErrorResponse.status == 401) {
              this.toastrService.error("Session Expired", "Please Login",  { positionClass: "toast-top-center" })
            }
          }
        });
    }
  }

  receiveLoginEvent(): void {
    this.isLoggedIn = JSON.parse(sessionStorage.getItem("isLoggedIn"));
  }

  receiveLogoutEvent(): void {
    this.isLoggedIn = false;
  }
}
