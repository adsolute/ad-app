import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginatedTableComponent } from './paginated-table.component';
import { CamelcaseToPhrasePipe } from './pipes/camelcase-to-phrase.pipe';
import { FormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { ConfirmActionDialogComponent } from './confirm-action-dialog.component';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SortByPipe } from './pipes/sort-by.pipe';

@NgModule({
  declarations: [PaginatedTableComponent, CamelcaseToPhrasePipe, ConfirmActionDialogComponent, SortByPipe],
  imports: [
    CommonModule,
    MatSelectModule,
    FormsModule,
    PdfJsViewerModule,
    MatDatepickerModule,
  ],
  exports: [
    PaginatedTableComponent,
    FormsModule,
    MatAutocompleteModule,
    MatSelectModule,
    NgxMaterialTimepickerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    PdfJsViewerModule,
    MatTooltipModule,
    SortByPipe,
  ]
})
export class SharedModule { }
