export class PagedResultSet<T> {
    page: number;
    size: number;
    totalCount: number;
    limit: number;
    results: T[];
}