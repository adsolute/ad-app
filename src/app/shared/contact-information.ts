export class ContactInformation {
    phoneNumber: string;
    emailAddress: string;
}
