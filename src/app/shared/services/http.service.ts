import { Injectable } from '@angular/core';
import { IHttpService } from 'src/app/interfaces/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { tap, catchError, finalize, delay } from 'rxjs/operators';
import { SpinnerOverlayService } from './spinner-overlay.service';
import { QueryParameters } from '../query-parameters';

@Injectable({
  providedIn: 'root'
})
export class HttpService<T> implements IHttpService<T>{

  stayLoggedInSource: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  stayLoggedInObervable: Observable<boolean> = this.stayLoggedInSource.asObservable();

  constructor(private httpClient: HttpClient, private spinnerOverlayService: SpinnerOverlayService) { }

  get(url: string): Observable< T[] | HttpResponse<T[]> | T | HttpResponse<T>> {
    this.spinnerOverlayService.hide();
    this.spinnerOverlayService.show();
    return this.httpClient.get<T[]>(url, {observe: 'response'}).pipe(
      tap(httpData => console.log("Retrieved Data ", JSON.stringify(httpData))),
      catchError((error) => this.handleError(error, this.stayLoggedInSource)),
      finalize(() => this.spinnerOverlayService.hide())
    );
  }

  getWithResponseType(url: string, responseType: string): Observable< T[] | HttpResponse<T[]> | T | HttpResponse<T>> {
    this.spinnerOverlayService.hide();
    this.spinnerOverlayService.show();
    return this.httpClient.get<T[]>(url, {responseType: responseType as 'json'}).pipe(
      tap(httpData => console.log("Retrieved Data ", JSON.stringify(httpData))),
      catchError((error) => this.handleError(error, this.stayLoggedInSource)),
      finalize(() => this.spinnerOverlayService.hide())
    );
  }

  post(url: string, payload: T): Observable< T | HttpResponse<T>> {
    this.spinnerOverlayService.hide();
    this.spinnerOverlayService.show();
    return this.httpClient.post<T>(url, payload, {observe: 'response'}).pipe(
      tap(httpData => console.log("Retrieved Data ", JSON.stringify(httpData))),
      catchError((error) => this.handleError(error, this.stayLoggedInSource)),
      finalize(() => this.spinnerOverlayService.hide())
    );
  }

  postWithResponseType(url: string, payload: T, responseType: string): Observable< T | HttpResponse<T>> {
    this.spinnerOverlayService.hide();
    this.spinnerOverlayService.show();
    return this.httpClient.post<T>(url, payload, {responseType: responseType as 'json'}).pipe(
      tap(httpData => console.log("Retrieved Data ", JSON.stringify(httpData))),
      catchError((error) => this.handleError(error, this.stayLoggedInSource)),
      finalize(() => this.spinnerOverlayService.hide())
    );
  }

  postQuery<P, R>(url: string, payload: P): Observable< R | HttpResponse<R>> {
    this.spinnerOverlayService.hide();
    this.spinnerOverlayService.show();
    return this.httpClient.post<R>(url, payload, {observe: 'response'}).pipe(
      tap(httpData => console.log("Retrieved Data ", JSON.stringify(httpData))),
      catchError((error) => this.handleError(error, this.stayLoggedInSource)),
      finalize(() => this.spinnerOverlayService.hide())
    );
  }

  postQueryWithNoOverlay<P, R>(url: string, payload: P): Observable< R | HttpResponse<R>> {
    return this.httpClient.post<R>(url, payload, {observe: 'response'}).pipe(
      tap(httpData => console.log("Retrieved Data ", JSON.stringify(httpData))),
      catchError((error) => this.handleError(error, this.stayLoggedInSource))
    );
  }

  put(url: string, payload: T): Observable< T | HttpResponse<T>> {
    this.spinnerOverlayService.hide();
    this.spinnerOverlayService.show();
    return this.httpClient.put<T>(url, payload, {observe: 'response'}).pipe(
      tap(httpData => console.log("Retrieved Data ", JSON.stringify(httpData))),
      catchError((error) => this.handleError(error, this.stayLoggedInSource)),
      finalize(() => this.spinnerOverlayService.hide())
    );
  }

  delete(url: string): Observable< T | HttpResponse<T>> {
    this.spinnerOverlayService.hide();
    this.spinnerOverlayService.show();
    return this.httpClient.delete<T>(url, {observe: 'response'}).pipe(
      tap(httpData => console.log("Retrieved Data ", JSON.stringify(httpData))),
      catchError((error) => this.handleError(error, this.stayLoggedInSource)),
      finalize(() => this.spinnerOverlayService.hide())
    );
  }

  private handleError(httpErrorResponse: HttpErrorResponse, stayLoggedInSource: BehaviorSubject<boolean>): Observable<never>{
    if(httpErrorResponse.status == 0){
      return throwError({
        error:"Failed to Connect.",
        status:0
      });
    }
    return throwError(httpErrorResponse);
  }
}
