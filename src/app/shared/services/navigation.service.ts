import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  @Output()
  activeBranchChangedEmitter: EventEmitter<string> = new EventEmitter();

  constructor() { }

  changeActiveBranch(branchId: string) {
    this.activeBranchChangedEmitter.emit(branchId);
  }
}
