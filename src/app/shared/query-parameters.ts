export class QueryParameters {
    criteria: Map<string, any> = new Map;
    page: number;
    limit: number;
    sortField: string;
    ascending: boolean;
    regex: boolean;
    dateRanges: DateRange[] = [];
}

export class DateRange {
    field: string;
    start: Date;
    end: Date;
}