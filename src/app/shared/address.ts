export class Address {
    addressLine1: string;
    addressLine2: string;
    city: string;
    zipCode: string;
    province: string;
    country: string;
}
