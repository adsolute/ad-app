
import { Action, Resource } from '../roles/role';

export class PermissionCheckUtils {
    static checkPermissions(resource: Resource, action: Action): boolean {
        let permissions = JSON.parse(sessionStorage.getItem("permissions"));
        return permissions
          .filter(permission => {
            let permitted = permission.resource.toUpperCase() == resource.toUpperCase()
            && permission.action.toUpperCase() == action.toUpperCase();
            let isAdmin = permission.resource.toUpperCase() == "ALL"
            && permission.action.toUpperCase() == "ALL";
    
            return permitted || isAdmin;
          })
          .length > 0;
      }
}