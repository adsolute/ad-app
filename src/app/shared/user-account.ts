import { GrantedRole } from '../roles/granted-role';
import { ContactInformation } from './contact-information';
import { PersonalDetails } from './personal-details';
import { Permission } from '../roles/role';

export class UserAccount {
    id: string;
    personalDetails: PersonalDetails = new PersonalDetails;
    contactInformation: ContactInformation = new ContactInformation;
    accountId: string;
    grantedRoles: GrantedRole[] = [];
    grantedPermissions: GrantedPermission[];
}

export class GrantedPermission {
    onAccountId: string;
    permission: Permission;
}