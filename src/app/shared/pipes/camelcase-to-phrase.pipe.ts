import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'camelcaseToPhrase'
})
export class CamelcaseToPhrasePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    var regex = /([A-Z])([A-Z])([a-z])|([a-z])([A-Z])/g;
    return value.replace( regex, '$1$4 $2$3$5' ).toUpperCase();
  }

}
