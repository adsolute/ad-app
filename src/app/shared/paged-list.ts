import { PagedResultSet } from './paged-result-set';
import { Action, Resource } from '../roles/role';

export class PagedList {
    getPages<T>(pagedResultSet: PagedResultSet<T>): number[] {
        let totalPage = Math.ceil(pagedResultSet.totalCount / pagedResultSet.limit);
        let pages: number[] = [];
        if(!isNaN(totalPage)) {
            for(let page=1; page<= totalPage; page++) {
                pages.push(page);
            }
        } else {
            pages.push(1);
        }
        return pages;
    }

    hideCreateButton(resource: Resource): boolean {
        let permissions = JSON.parse(sessionStorage.getItem("permissions"));
        return permissions
          .filter(permission => {
            let permitted = permission.resource.toUpperCase() == resource.toUpperCase()
            && permission.action.toUpperCase() == Action.CREATE.toUpperCase();
            let isAdmin = permission.resource.toUpperCase() == "ALL"
            && permission.action.toUpperCase() == "ALL";
    
            return permitted || isAdmin;
          })
          .length == 0;
      }
}