import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PagedResultSet } from './paged-result-set';
import * as moment from 'moment';
import { QueryParameters } from './query-parameters';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-paginated-table',
  templateUrl: './paginated-table.component.html',
  styleUrls: ['./paginated-table.component.css']
})
export class PaginatedTableComponent<T> implements OnInit {

  @Input()
  routeUrl: string;
  @Input()
  tableTitle: string;
  @Input()
  pagedResultSet: PagedResultSet<T>;
  @Input()
  headers: [];
  @Input()
  pages: number[];
  limits: number[] = [50, 100, 200];
  @Output()
  queryParametersChanged: EventEmitter<null> = new EventEmitter();
  @Input()
  queryParameters: QueryParameters;
  @Input()
  tableActions: TableAction[];
  @Input()
  keyId: string;
  @Input()
  typeCriteria: TypeCriteria;
  @Input()
  filters: Filter[];
  @Input()
  dateRanges: any[];
  @Input()
  rowColorIdentifier: string;
  @Input()
  rowColor: string;
  @Output()
  updateButtonClicked: EventEmitter<string> = new EventEmitter();
  @Output()
  specialButtonClicked: EventEmitter<string> = new EventEmitter();
  @Output()
  deleteButtonClicked: EventEmitter<string> = new EventEmitter();
  @Output()
  popupButtonClicked: EventEmitter<string> = new EventEmitter();
  withFilters: boolean = false;
  type: string;
  searchFilter: Filter;
  dateRangeFilter: DateRangeFilter;

  constructor(
    private router: Router,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.dateRanges.unshift("");
  }

  getValue(object: any, header: Header): any{
    let hasHyphen = false;
    let notEndingWithFiveNumerics = false;
    if(typeof object[header.actual] === "string") {
      let value: string = object[header.actual];
      hasHyphen = value.includes("-");

      if(hasHyphen) {
        let delimitedValues: string[] = value.split("-");
        notEndingWithFiveNumerics = 
          delimitedValues[delimitedValues.length-1].length != 5;
      }
    }

    if(header.isADateField) {
      if(object[header.actual] != null) {
        return moment(object[header.actual]).format("MMM-DD-YYYY hh:mma");
      }
    }
    return object[header.actual];
  }

  getNumberValue(object: any, header: Header): number{
    return object[header.actual];
  }

  isANumber(object: any, header: Header): boolean{
    return typeof object[header.actual] === "number";
  }
  
  openItem(item: T): void {
    console.log("Item clicked with id: ", this.routeUrl, item[this.keyId]);
    this.router.navigate([this.routeUrl, item[this.keyId]]);
  }

  openItemInNewTab(item: T): void {
    console.log("Item clicked with id: ", this.routeUrl, item[this.keyId]);
    // this.router.navigate([this.routeUrl, item[this.keyId]]);
    const url = this.router.serializeUrl(
      this.router.createUrlTree([this.routeUrl, item[this.keyId]])
    );
    window.open(url);
  }

  limitChanged(limit: number) {
    this.queryParameters.page = 1;
    this.queryParameters.limit = limit;
    this.queryParametersChanged.emit();
  }

  pageChanged(page: number) {
    this.queryParameters.page = page;
    this.queryParametersChanged.emit();
  }

  emitUpdateButtonClicked(id: string) {
    this.updateButtonClicked.emit(id);
  }

  emitSpecialButtonClickedEvent(id: string) {
    this.specialButtonClicked.emit(id);
  }
  
  emitDeleteButtonClickedEvent(id: string) {}

  emitPopupButtonClickedEvent(id: string) {}

  sendQueryWithFilters() {
    this.queryParameters.criteria = new Map;
    if(this.type) {
      this.queryParameters.criteria[this.typeCriteria.key] = this.type;
    }
    if (this.searchFilter && this.searchFilter.value!="") {
      this.queryParameters.criteria[this.searchFilter.key] = this.searchFilter.value;
      this.queryParameters.regex = true
    }
    if (this.dateRangeFilter) {
      if(this.dateRangeFilter.start && this.dateRangeFilter.end) {
        this.dateRangeFilter.start.setHours(0, 0, 0);
        this.dateRangeFilter.end.setHours(23, 59, 59);
        this.queryParameters.dateRanges = [{
          field: this.dateRangeFilter.key,
          start: this.dateRangeFilter.start,
          end: this.dateRangeFilter.end
        }];
      } else {
        this.toastrService.warning("Deselect 'Range By' if not needed", "Start and End Dates are required", { positionClass: "toast-top-center" })
        return;
      }
    }
    this.queryParametersChanged.emit();
  }

  clearFilters() {
    this.queryParameters.criteria = new Map;
    this.queryParameters.dateRanges = 
    this.searchFilter = undefined;
    this.dateRangeFilter = undefined;
    this.type = undefined;
    this.filters.map(filter => delete filter.value);
    this.dateRanges.map(dateRange => {
      delete dateRange.start;
      delete dateRange.end;
    });
    this.queryParametersChanged.emit(); 
  }

  toggleSort(header: Header) {
    if(header.sortable) {
      if(this.queryParameters.sortField != header.actual) {
        this.queryParameters.sortField = header.actual;
        this.queryParameters.ascending = false;
      }
      this.queryParameters.ascending = !this.queryParameters.ascending;
      this.queryParametersChanged.emit();
    }
  }

  checkColor(object: any, header: Header){
    if(object[header.actual] == "PAID"){
      return "#00CC00";
    }
    if(object[header.actual] == "PARTIAL"){
      return "#FFFF99";
    }
    if(object[header.actual] == "UNPAID"){
      return "#D3D3D3";
    }
  }

  checkRowColor(object: any){
    if(object[this.rowColorIdentifier]){
      return this.rowColor;
    }
  }
}

export class TableAction {
  type: TableActionType;
  text: string;
}

export enum TableActionType {
  Update="UPDATE",
  Special="SPECIAL",
  Delete="DELETE",
  Popup="POPUP"
}

export class TypeCriteria {
  key: string;
  types: any[];
}

export class Filter {
  name: string;
  key: string;
  value?: string = null;
}

export class DateRangeFilter {
  name: string;
  key: string;
  start?: Date = null;
  end?: Date = null;
}

export class Header {
  display: string;
  actual: string;
  sortable: boolean;
  isADateField? : boolean;
}