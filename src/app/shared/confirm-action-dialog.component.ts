import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './confirm-action-dialog.component.html',
  styleUrls: ['./confirm-action-dialog.component.css']
})
export class ConfirmActionDialogComponent implements OnInit {

  actionToConfirm: string;
  constructor(
    private dialogRef: MatDialogRef<ConfirmActionDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    private data: any) { 
      this.actionToConfirm = data.action;
    }

  ngOnInit() {
  }

  confirmDeletion() {
    this.dialogRef.close({event:"Confirm"});
  }

  cancelDeletion() {
    this.dialogRef.close();
  }
}
