import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchasesRoutingModule } from './purchases-routing.module';
import { PurchasesListComponent } from './purchases-list.component';



@NgModule({
  declarations: [PurchasesListComponent],
  imports: [
    CommonModule,
    PurchasesRoutingModule,
  ]
})
export class PurchasesModule { }
