import { Header } from '../shared/paginated-table.component';

export interface PaginatedList {
    getHeaders: (any) => Header[];
    getPages: (any) => number[];
    sendQueryParameters: (any) => void;
    hideCreateButton: () => boolean;
}