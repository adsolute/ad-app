import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

export interface IHttpService<T> {
    get(url: string): Observable< T[] | HttpResponse<T[]> | T | HttpResponse<T>>;
    post(url: string, payload: T): Observable< T | HttpResponse<T>>;
    postQuery<P, R>(url: string, payload: P): Observable< R[] | HttpResponse<R[]>>;
    postQueryWithNoOverlay<P, R>(url: string, payload: P): Observable< R[] | HttpResponse<R[]>>;
    put(url: string, payload: T): Observable< T | HttpResponse<T>>;
    delete(url: string): Observable< T | HttpResponse<T>>;
}