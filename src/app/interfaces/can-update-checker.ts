export interface CanUpdateChecker {
    checkCanUpdate(): boolean; 
}