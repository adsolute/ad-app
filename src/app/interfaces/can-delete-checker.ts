export interface CanDeleteChecker {
    checkCanDelete(): boolean; 
}