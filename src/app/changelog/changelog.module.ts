import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangelogComponent } from './changelog.component';
import { NgxMdModule } from 'ngx-md';


@NgModule({
  declarations: [ChangelogComponent],
  imports: [
    NgxMdModule.forRoot(),
    CommonModule
  ]
})
export class ChangelogModule { }
