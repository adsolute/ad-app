import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ChangelogComponent } from './changelog.component';

const routes: Routes = [
  {path:"changelog", component: ChangelogComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)]
})
export class ChangelogRoutingModule { }
