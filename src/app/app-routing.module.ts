import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicDocumentComponent } from './public/public-document.component';


const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "public/reports/document/:shit", component: PublicDocumentComponent},
  { path: "**", redirectTo: "home", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
