import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScheduleComponent } from './schedule.component';
import { ScheduleRoutingModule } from './schedule-routing.module';
import { SchedulerModule } from '@progress/kendo-angular-scheduler';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleTimer } from 'ng2-simple-timer';

@NgModule({
  providers: [SimpleTimer],
  declarations: [ScheduleComponent],
  imports: [
    CommonModule,
    ScheduleRoutingModule,
    SchedulerModule,
    BrowserAnimationsModule,
  ]
})
export class ScheduleModule { }
