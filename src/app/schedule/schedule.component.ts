import { Component, OnInit } from '@angular/core';
import { EventStyleArgs, DateChangeEvent, EventClickEvent } from '@progress/kendo-angular-scheduler';
import { InvoiceSchedule } from './invoice-schedule';
import { HttpService } from '../shared/services/http.service';
import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { QueryParameters, DateRange } from '../shared/query-parameters';
import { environment } from 'src/environments/environment';
import { SimpleTimer } from 'ng2-simple-timer';
import { Resource } from '../roles/role';
import { Router } from '@angular/router';

@Component({
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  selectedDate: Date = new Date();
  workDayStart: Date = new Date("2020/1/1 10:00 AM");
  workDayEnd: Date = new Date("2020/1/1 10:30 PM");
  invoiceSchedules: InvoiceSchedule[] = [];
  queryParameters: QueryParameters = new QueryParameters;

  getEventStyles = (args: EventStyleArgs) => {
    let style: any = {color: "black"};
    if(args.event.dataItem.forSameDayDelivery) {
      style = { border: '2px solid', "border-color":  "RED", color: "black"};
    }
    if(args.event.dataItem.status === environment.initialDeliveryStatus) {
      style.backgroundColor = "#D3D3D3";
    }
    if(args.event.dataItem.status === environment.processingStatus) {
      style.backgroundColor = "#FFFF99";
    }
    if(args.event.dataItem.status === environment.deliveredStatus) {
      style.backgroundColor = "#00CC00";
    }
    if(args.event.dataItem.status === environment.forPickUpStatus) {
      style.backgroundColor = "#0584e3";
    }
    if(args.event.dataItem.status === environment.doneStatus) {
      style.backgroundColor = "#lightpink";
    }
    return style;
  }

  constructor(
    private httpService: HttpService<InvoiceSchedule>,
    private simpleTimer: SimpleTimer,
    private toastrService: ToastrService,
    private router: Router) {
      simpleTimer.delTimer("checkUpdates");
      simpleTimer.newTimer("checkUpdates", 15);
  }

  ngOnInit() {
    this.simpleTimer.subscribe("checkUpdates", () => {
      let urlParameters: string[] = location.pathname.split("/");
      if(urlParameters[urlParameters.length - 1].toUpperCase() == Resource.SCHEDULES.toUpperCase()) {
        this.getInvoiceSchedules();
      }
    });
  }

  ngOnDestroy() {
    console.log("Destroying timer for checkUpdates");
    this.simpleTimer.delTimer("checkUpdates");
  }

  changeInvoiceScheduleRange(dateChangedEvent: DateChangeEvent) {
    this.queryParameters.dateRanges = [];
    let dateRange: DateRange = new DateRange;
    dateRange.field = "deliveryDate";
    dateRange.start = dateChangedEvent.dateRange.start;
    dateRange.end = dateChangedEvent.dateRange.end;

    this.queryParameters.dateRanges.push(dateRange);
    this.getInvoiceSchedules();
  }

  getInvoiceSchedules() {
    this.queryParameters.criteria["voided"]=false;
    this.httpService.postQuery<QueryParameters, InvoiceSchedule[]>(environment.backendApiUrl+"/invoices/schedules?branch-id=" + localStorage.getItem("activeBranchId"), this.queryParameters)
    .subscribe({
      next: (httpResponse: HttpResponse<InvoiceSchedule[]>) => {
        this.invoiceSchedules = httpResponse.body;
        this.invoiceSchedules.map((invoiceSchedule) => {
          invoiceSchedule.start = new Date(invoiceSchedule.start);
          invoiceSchedule.end = new Date(invoiceSchedule.end)
        })
      },
      error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }

  viewInvoice(eventClickEvent: EventClickEvent) {
    console.log(eventClickEvent);
    this.router.navigate(['invoices/view', eventClickEvent.event.dataItem.id]);
  }
}
