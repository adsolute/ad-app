export class InvoiceSchedule {
    id: string;
    title: string;
    description: string;
    start: Date;
    end: Date;
    status: string;
    city: string;
}