import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuditsComponent } from './audits.component';
import { ViewAuditComponent } from './view-audit.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: "audits", component: AuditsComponent },
      { path: "audits/view/:id", component: ViewAuditComponent },
    ]),
  ]
})
export class AuditsRoutingModule { }
