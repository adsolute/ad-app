import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuditsComponent } from './audits.component';
import { SharedModule } from '../shared/shared.module';
import { ViewAuditComponent } from './view-audit.component';



@NgModule({
  declarations: [
    AuditsComponent,
    ViewAuditComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class AuditsModule { }
