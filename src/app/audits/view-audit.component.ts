import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { HttpService } from '../shared/services/http.service';

@Component({
  selector: 'app-view-audit',
  templateUrl: './view-audit.component.html',
  styleUrls: ['./view-audit.component.css']
})
export class ViewAuditComponent implements OnInit {

  audit: any;

  constructor(
    private httpService: HttpService<any>,
    private activatedRoute: ActivatedRoute,
    private toastrService: ToastrService, ) {}

  ngOnInit(): void {
    let id: string = this.activatedRoute.snapshot.paramMap.get("id");
    this.httpService.get(environment.auditApiUrl+"/audits?id=" + id + "&branch-id=" + localStorage.getItem("activeBranchId"))
      .subscribe({
        next: (httpResponse: HttpResponse<any>) => {
          this.audit = httpResponse.body;
          console.log(this.audit);
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

}
