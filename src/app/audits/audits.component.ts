import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { PaginatedList } from '../interfaces/paginated-list';
import { Resource } from '../roles/role';
import { PagedList } from '../shared/paged-list';
import { PagedResultSet } from '../shared/paged-result-set';
import { DateRangeFilter, Filter, Header, TableAction } from '../shared/paginated-table.component';
import { QueryParameters } from '../shared/query-parameters';
import { HttpService } from '../shared/services/http.service';

@Component({
  selector: 'app-audits',
  templateUrl: './audits.component.html',
  styleUrls: ['./audits.component.css']
})
export class AuditsComponent extends PagedList implements OnInit, PaginatedList  {

  auditPagedResult: PagedResultSet<any> = new PagedResultSet;
  queryParameters: QueryParameters = new QueryParameters;

  constructor(private httpService: HttpService<any>, 
    private toastrService: ToastrService,) { 
      super();
    }

  ngOnInit(): void {
    this.queryParameters.limit = 50;
    this.queryParameters.page = 1;
    this.queryParameters.sortField = "auditDate";
    this.queryParameters.ascending = false;
    this.sendQueryParameters();
  }

  hideCreateButton() {
    return true;
  }

  getHeaders(): Header[] {
    return [
      {display:"date", actual:"auditDate", sortable: true, isADateField: true},
      {display:"user", actual:"username", sortable: true},
      {display:"resource", actual:"resource", sortable: true},
      {display:"action", actual:"auditAction", sortable: true},
      {display:"identifiedBy", actual:"identifierName", sortable: true},
      {display:"identifierValue", actual:"identifier", sortable: true},
    ];
  }

  filters: Filter[] = [
  ];
  
  dateRanges: DateRangeFilter[] = [
  ];

  getPages(): number[] {
    return super.getPages(this.auditPagedResult);
  }

  getTableActions(): TableAction[] {
    let tableActions: TableAction[] = [];
    return tableActions
  }

  sendQueryParameters() {
    console.log(this.queryParameters);
    this.httpService.postQuery<QueryParameters, PagedResultSet<any>>(environment.auditApiUrl+"/audits/query?branch-id=" + localStorage.getItem("activeBranchId"), this.queryParameters)
      .subscribe({
        next: (httpResponse: HttpResponse<PagedResultSet<any>>) => this.auditPagedResult = httpResponse.body,
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }
}
