import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RolesListComponent } from './roles-list.component';
import { SharedModule } from '../shared/shared.module';
import { CreateRoleComponent } from './create-role.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@NgModule({
  declarations: [RolesListComponent, CreateRoleComponent],
  imports: [
    CommonModule,
    SharedModule,
    MatSlideToggleModule
  ]
})
export class RolesModule { }
