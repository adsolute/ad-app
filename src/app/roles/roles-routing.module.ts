import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { RolesListComponent } from './roles-list.component';
import { CreateRoleComponent } from './create-role.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: "roles", component: RolesListComponent },
      { path: "roles/create", component: CreateRoleComponent },
      { path: "roles/view/:id", component: CreateRoleComponent },
    ]),
  ]
})
export class RolesRoutingModule { }
