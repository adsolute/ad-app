import { Component, OnInit } from '@angular/core';
import { PaginatedList } from '../interfaces/paginated-list';
import { HttpService } from '../shared/services/http.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Role, Resource } from './role';
import { HttpResponse } from '@angular/common/http';
import { QueryParameters } from '../shared/query-parameters';
import { PagedResultSet } from '../shared/paged-result-set';
import { PagedList } from '../shared/paged-list';
import { environment } from 'src/environments/environment';
import { Header, Filter, DateRangeFilter } from '../shared/paginated-table.component';

@Component({
  templateUrl: './roles-list.component.html',
  styleUrls: ['./roles-list.component.css']
})
export class RolesListComponent extends PagedList implements OnInit, PaginatedList {

  rolePagedResultSet: PagedResultSet<Role> = new PagedResultSet;
  queryParameters: QueryParameters = new QueryParameters;

  constructor(
    private httpService: HttpService<Role>,
    private toastrService: ToastrService,
    private router: Router) { 
      super();
    }

  ngOnInit() {
    this.queryParameters.page = 1;
    this.queryParameters.limit = 50;
    this.sendQueryParameters();
  }

  getPages = (): number[] => {
    return super.getPages(this.rolePagedResultSet);
  }
  
  sendQueryParameters() {
    this.httpService.postQuery<QueryParameters, PagedResultSet<Role>>(environment.iamApiUrl+"/roles/query?branch-id=" + localStorage.getItem("activeBranchId"), this.queryParameters)
      .subscribe({
        next: (httpResponse: HttpResponse<PagedResultSet<Role>>) => this.rolePagedResultSet = httpResponse.body,
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  getHeaders(): Header[] {
    return [
      {display: "name", actual: "name", sortable: true},
      {display: "createDate", actual: "createDate", sortable: true, isADateField: true},
      {display: "updateDate", actual: "updateDate", sortable: true, isADateField: true},
    ];
  }

  filters: Filter[] = [
    { name: "Name", key: "name"},
    { name: "Description", key: "description"},
  ];
  
  dateRanges: DateRangeFilter[] = [
    { name: "Create Date", key: "createDate"},
    { name: "Update Date", key: "updateDate"},
  ];

  goToCreateRoleLink() {
    this.router.navigate(["roles/create"]);
  }

  hideCreateButton = () => super.hideCreateButton(Resource.ROLES);
}
