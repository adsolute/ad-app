export class GrantedRole {
    onAccountId: string;
    onAccountName: string;
    roleId: string;
    roleName: string
}