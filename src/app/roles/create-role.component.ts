import { Component, OnInit } from '@angular/core';
import { Role, Resource, Action, Permission } from './role';
import { HttpService } from '../shared/services/http.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpResponse } from '@angular/common/http';
import { CanUpdateChecker } from '../interfaces/can-update-checker';
import { PermissionCheckUtils } from '../shared/permission-check-utils';
import { KeyValue } from '@angular/common';

@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.css']
})
export class CreateRoleComponent implements OnInit, CanUpdateChecker {

  role: Role = new Role();
  resources: string[] = Object.keys(Resource);
  actions: string[] = Object.keys(Action);
  permissions: Permission[] = new Array();
  viewMode: boolean = false;
  canUpdate: boolean = false;
  headerMode: string = "Add";
  riderTooltipMessage: string = "This role can be used for accounts that are meant for people who will do the delivery. Accounts with this role will show up as an option in the 'RIDER DETAILS' field inside the INVOICE";

  constructor(
    private httpService: HttpService<Role>,
    private toastrService: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
      if("view" == activatedRoute.snapshot.url[1].path) {
        this.viewMode = true;
        let id: string = this.activatedRoute.snapshot.paramMap.get("id");
        this.httpService.get(environment.iamApiUrl+"/roles/role?id="+id+"&branch-id="+localStorage.getItem("activeBranchId")).subscribe({
          next : (httpResponse: HttpResponse<Role>) => {
            this.role = httpResponse.body;
            this.permissions = this.role.permissions;
          },
          error: error => {
            this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" });
            // this.router.navigate(["roles"]);
          }
        });
      }
    }

  ngOnInit() {
    this.canUpdate = this.checkCanUpdate();
  }

  saveRole() {
    this.role.permissions = this.permissions;
    if (this.role.id == null) {
      this.createRole();
    } else {
      this.updateRole();
    }
  }

  createRole() {
    this.httpService.post(environment.iamApiUrl+"/roles?branch-id="+localStorage.getItem("activeBranchId"), this.role).subscribe({
      next: () => {
        this.toastrService.success("Role Created", "Success", { positionClass: "toast-top-center" });
        this.router.navigate(["roles"]);
      },
      error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }

  updateRole() {
    this.httpService.put(environment.iamApiUrl+"/roles?branch-id="+localStorage.getItem("activeBranchId"), this.role).subscribe({
      next: () => {
        this.toastrService.success("Role Updated", "Success", { positionClass: "toast-top-center" });
        this.router.navigate(["roles"]);
      },
      error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }

  actionToggled(checked: boolean, resource: Resource, action: Action) {
    if(checked) {
      this.permissions.push(new Permission(resource, action));
    } else {
      this.permissions = this.permissions.filter(permission => {
        return !(resource === permission.resource && action === permission.action)
      })
    }
    console.log(this.permissions);
  }

  hideActionToggle(resource: Resource, action: Action): boolean {
    console.log(resource, action);
    if(resource==Resource.DELIVERY_STATUS.toUpperCase() && action!=Action.UPDATE.toUpperCase()) {
      return true;
    } else if(resource==Resource.INVOICES_VOID.toUpperCase() && action!=Action.UPDATE.toUpperCase()) {
      return true;
    } else if(resource==Resource.RIDER_DETAILS.toUpperCase() && action!=Action.UPDATE.toUpperCase()) {
      return true;
    } else if(resource==Resource.PAYMENT_DETAILS.toUpperCase() && action!=Action.UPDATE.toUpperCase()) {
      return true;
    } else if((resource==Resource.PROCESSING_QUEUE.toUpperCase() || resource==Resource.PICKUP_QUEUE.toUpperCase()) && action!=Action.READ.toUpperCase() && action!=Action.UPDATE.toUpperCase()) {
      return true;
    } else if(resource.includes("AUDIT") && action!=Action.READ.toUpperCase()) {
      return true;
    }
    return false;
  }

  checkCanUpdate():boolean {
    return PermissionCheckUtils.checkPermissions(Resource.ROLES, Action.UPDATE);
  }

  hasPermission(resource: Resource, action: Action): boolean {
    return this.permissions.filter(permission => {
      return permission.resource === resource && permission.action === action
    }).length > 0;
  }
  
  delete() {
    
  }

  toggleForRider(checked: boolean) {
    this.role.otherProperties["forRider"] = checked;
  }

  valueAscOrder = (
    a: KeyValue<number, string>,
    b: KeyValue<number, string>
  ): number => {
    return a.value.localeCompare(b.value);
  };
}
