export class Role {
    id: string;
    roleId: string;
    name: string;
    description: string;
    permissions: Permission[]
    otherProperties: Map<string, any> = new Map();
}

export class Permission {
    resource: Resource;
    action: Action;

    constructor(resource: Resource, action: Action) {
        this.resource = resource;
        this.action = action;
    }
}

export enum Action {
    CREATE="Create",
    READ="Read",
    UPDATE="Update",
    DELETE="Delete"
}

export enum Resource {
    INVOICES="Invoices",
    INVOICES_AUDIT="Invoices_Audit",
    INVOICES_VOID="Invoices_Void",
	PRODUCTS="Products",
	PRODUCTS_AUDIT="Products_Audit",
	CUSTOMERS="Customers",
	CUSTOMERS_AUDIT="Customers_Audit",
	PURCHASES="Purchases",
	REPORTS="Reports",
    SCHEDULES="Schedules",
    RIDER_DETAILS="Rider_Details",
    PAYMENT_DETAILS="Payment_Details",
	PROCESSING_QUEUE="Processing_Queue",
	PICKUP_QUEUE="Pickup_Queue",
    ACCOUNTS="Accounts",
    ACCOUNTS_AUDIT="Accounts_Audit",
    ROLES="Roles",
    ROLES_AUDIT="Roles_Audit",
    BRANCHES="Branches",
    BRANCHES_AUDIT="Branches_Audit",
    DELIVERY_STATUS="Delivery_Status",
}