import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { CountUpModule } from 'ngx-countup';



@NgModule({
  declarations: [HomeComponent],
  imports: [
    MatGridListModule,
    CountUpModule,
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
