import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { Invoice } from '../invoices/invoice';
import { HttpService } from '../shared/services/http.service';
import { NavigationService } from '../shared/services/navigation.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title: string = environment.brandName;
  breakpoint: any;
  showDashboard: boolean;
  deliveriesToday: number;
  sameDayDeliveries: number;
  deliveredToday: number;
  remainingDeliveries: number;

  constructor(
    private httpService: HttpService<Invoice>,
    private toastrService: ToastrService,
    private navigationService: NavigationService) { }

  ngOnInit() {
    this.breakpoint = (window.innerWidth <= 750) ? 1 : 2;
    this.getDashboardInvoiceDeliveries(localStorage.getItem("activeBranchId"));
    this.navigationService.activeBranchChangedEmitter.subscribe((branchId: string) => {
      this.getDashboardInvoiceDeliveries(branchId);
    });
  }

  getDashboardInvoiceDeliveries(branchId: string) {
    this.httpService.get(environment.backendApiUrl+"/invoices/deliveries/dashboard?branch-id=" + branchId)
        .subscribe({
          next: (httpResponse: HttpResponse<any>) => {
            this.showDashboard = true;
            this.deliveriesToday = httpResponse.body.deliveriesToday;
            this.sameDayDeliveries = httpResponse.body.sameDayDeliveries;
            this.deliveredToday = httpResponse.body.deliveredToday;
            this.remainingDeliveries = httpResponse.body.remainingDeliveries;
          },
          error: (httpErrorResponse: HttpErrorResponse) => {
            this.showDashboard = false;
            if(httpErrorResponse.status == 401) {
              this.toastrService.info("Need Invoice Read permission to view deliveries in dashboard", "Dashboard not displayed",  { positionClass: "toast-top-center" })
            }
          }
        });
  }

  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 750) ? 1 : 2;
  }

}
