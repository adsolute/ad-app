import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryStatusDialogComponent } from './delivery-status-dialog.component';

describe('DeliveryStatusDialogComponent', () => {
  let component: DeliveryStatusDialogComponent;
  let fixture: ComponentFixture<DeliveryStatusDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryStatusDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryStatusDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
