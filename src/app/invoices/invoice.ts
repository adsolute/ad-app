import { Customer } from '../customers/customer';
import { Product } from '../products/product';
import { PersonalDetails } from '../shared/personal-details';

export class Invoice {
    id: string;
    deliveryStatus: string;
    due: Date;
    date: Date;
    number: string;
    customer: Customer;
    amountDue: number;
    discount: number;
    items: Item[] = [];
    deliveryDate: Date
    notes: Note[] = [];
    branchId: string;
    transactionType: TransactionType;
    tableNumber: string
    riderDetails: RiderDetails;
    paymentDetails: PaymentDetails;
    paymentStatus: PaymentStatus;
    invoiceDate: Date;
    deliveryCharge: any;
    voided: boolean;
    withServiceCharge: boolean;
    withDiscount: boolean;
    serviceCharge: number;
    remarks: string;
    numberOfPax: number;
    
}

export class Item {
    quantity: number;
    product: Product;
}

export class Note {
    type: string;
    message: string; 
}

export enum TransactionType {
    DINE_IN="DINE_IN",
    DELIVERY="DELIVERY",
    TAKE_OUT="TAKE_OUT"
}

export class RiderDetails {
    personalDetails: PersonalDetails;
    accountId: string;
}

export class PaymentDetails {
    amount: number;
    note: string;
    paymentDate: Date;
    updateDate: Date;
    paymentMethod: PaymentMethod;
    paymentStatus; PaymentStatus;
}

export enum PaymentMethod {
    CASH="CASH",
    GCASH="GCASH",
    BANK_TRANSFER="BANK_TRANSFER",
    FOOD_PANDA="FOOD_PANDA",
    PETTY_CASH="PETTY_CASH",
    LALAMOVE="LALAMOVE",
    GRAB="GRAB",
    METRO_DEAL="METRO_DEAL",
    OTHER="OTHER",
}

export enum PaymentStatus {
    PAID="PAID",
    PARTIAL="PARTIAL",
    UNPAID="UNPAID"
}