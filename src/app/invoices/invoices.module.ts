import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicesListComponent } from './invoices-list.component';
import { InvoicesRoutingModule } from './invoices-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CreateInvoiceComponent } from './create-invoice.component';
import { DeliveryStatusDialogComponent } from './delivery-status-dialog.component';
import { PaymentDetailsDialogComponent } from './payment-details-dialog.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';



@NgModule({
  declarations: [InvoicesListComponent, CreateInvoiceComponent, DeliveryStatusDialogComponent, PaymentDetailsDialogComponent],
  imports: [
    CommonModule,
    InvoicesRoutingModule,
    SharedModule,
    MatSlideToggleModule
  ]
})
export class InvoiceModule { }
