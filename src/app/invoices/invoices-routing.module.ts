import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { InvoicesListComponent } from './invoices-list.component';
import { CreateInvoiceComponent } from './create-invoice.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: "invoices", component: InvoicesListComponent },
      { path: "invoices/create", component: CreateInvoiceComponent },
      { path: "invoices/view/:id", component: CreateInvoiceComponent },
    ]),
  ]
})
export class InvoicesRoutingModule { }
