import { Component, OnInit } from '@angular/core';
import { Invoice, TransactionType } from './invoice';
import { PaginatedList } from '../interfaces/paginated-list';
import { HttpService } from '../shared/services/http.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { QueryParameters } from '../shared/query-parameters';
import { PagedResultSet } from '../shared/paged-result-set';
import { PagedList } from '../shared/paged-list';
import { Resource, Action } from '../roles/role';
import { environment } from 'src/environments/environment';
import { TableAction, Filter, Header, DateRangeFilter, TypeCriteria, TableActionType } from '../shared/paginated-table.component';
import { PermissionCheckUtils } from '../shared/permission-check-utils';
import { MatDialog } from '@angular/material/dialog';
import { DeliveryStatusDialogComponent } from './delivery-status-dialog.component';
import { PaymentDetailsDialogComponent } from './payment-details-dialog.component';

@Component({
  templateUrl: './invoices-list.component.html',
  styleUrls: ['./invoices-list.component.css']
})
export class InvoicesListComponent extends PagedList implements OnInit, PaginatedList {

  invoicePagedResult: PagedResultSet<Invoice> = new PagedResultSet;
  queryParameters: QueryParameters = new QueryParameters;
  hasCreatePermission: boolean = false;

  getHeaders(): Header[] {
    return [
      {display:"paymentStatus", actual:"paymentStatus", sortable: true},
      {display:"number", actual:"number", sortable: true},
      {display:"status", actual:"status", sortable: true},
      {display:"transactionType", actual:"transactionType", sortable: true},
      {display:"tableNumber", actual:"tableNumber", sortable: true},
      {display:"customer", actual:"customer", sortable: true},
      {display:"amountDue", actual:"amountDue", sortable: true},
      {display:"updateDate", actual:"updateDate", sortable: true, isADateField:true},
      {display:"updatedBy", actual:"updatedBy", sortable: true},
    ];
  }
  
  typeCriteria: TypeCriteria = {
    key: "transactionType",
    types: [TransactionType.DINE_IN, TransactionType.DELIVERY]
  };

  filters: Filter[] = [
    { name: "Customer", key: "customer.name"},
    { name: "Invoice Number", key: "number"},
  ];
  
  dateRanges: DateRangeFilter[] = [
    { name: "Invoice Date", key: "invoiceDate"},
    { name: "Delivery Date", key: "deliveryDate"},
  ];

  getTableActions(): TableAction[] {
    let tableActions: TableAction[] = [];
    if(PermissionCheckUtils.checkPermissions(Resource.DELIVERY_STATUS, Action.UPDATE)) {
      tableActions.push({type: TableActionType.Update, text: ""})
      tableActions.push({type: TableActionType.Special, text: ""});
    }
    return tableActions
  }

  getPages(): number[] {
    return super.getPages(this.invoicePagedResult);
  }

  constructor(private httpService: HttpService<Invoice>, private toastrService: ToastrService,
    private router: Router,
    private matDialog: MatDialog) { 
      super();
    }

  ngOnInit() {
    this.queryParameters.limit = 50;
    this.queryParameters.page = 1;
    this.queryParameters.sortField = "createDate";
    this.queryParameters.ascending = false;
    this.sendQueryParameters();
  }

  goToCreateInvoiceLink(): void {
    this.router.navigate(["invoices/create"]);
  }

  sendQueryParameters() {
    console.log(this.queryParameters);
    this.httpService.postQuery<QueryParameters, PagedResultSet<Invoice>>(environment.backendApiUrl+"/invoices/query?branch-id=" + localStorage.getItem("activeBranchId"), this.queryParameters)
      .subscribe({
        next: (httpResponse: HttpResponse<PagedResultSet<Invoice>>) => this.invoicePagedResult = httpResponse.body,
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  hideCreateButton = () => super.hideCreateButton(Resource.INVOICES);
  
  openDeliveryStatusDialog(id: string){
    const dialogRef = this.matDialog.open(DeliveryStatusDialogComponent, {
      width: '500px', data: id
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.sendQueryParameters();
      }
    });
  }

  openPaymentDetailsDialog(id: string){
    const dialogRef = this.matDialog.open(PaymentDetailsDialogComponent, {
      width: '500px', data: id
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.sendQueryParameters();
      }
    });
  }
}
