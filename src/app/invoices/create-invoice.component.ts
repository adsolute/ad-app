import { Component, OnInit, ViewChild } from '@angular/core';
import { Invoice, Note, TransactionType, RiderDetails } from './invoice';
import { HttpService } from '../shared/services/http.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Customer } from '../customers/customer';
import { Product, ProductType } from '../products/product';
import { HttpResponse } from '@angular/common/http';
import { Branch } from '../branches/branch';
import { environment } from 'src/environments/environment';
import { CanUpdateChecker } from '..//interfaces/can-update-checker';
import { Resource, Action } from '../roles/role';
import { PermissionCheckUtils } from '../shared/permission-check-utils';
import { MatDialog } from '@angular/material/dialog';
import { CreateCustomerDialogComponent } from '../customers/create-customer-dialog.component';
import { ConfirmActionDialogComponent } from '../shared/confirm-action-dialog.component';
import { UserAccount } from '../shared/user-account';
import * as moment from 'moment';
import { NumberLiteralType } from 'typescript';

const INITIAL_QUANTITY = 1;
@Component({
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.css']
})
export class CreateInvoiceComponent implements OnInit, CanUpdateChecker {

  invoice: Invoice = new Invoice();
  products: Product[];
  customers: Customer[];
  validInvoice: boolean = false;
  totalAmountDue: number = 0;
  discount: number;
  minimumDeliveryDate: Date = new Date();
  deliveryDate: Date = new Date();
  deliveryTime: String;
  notes: Note[] = [
    { type:"Driver", message:""} ,
    { type:"Special Request", message:"" },
    { type:"Others", message:"" }
  ];
  selectedNote: Note;
  branches: Branch[] = [];
  isAdmin: boolean = false;
  tables: string[] = [undefined];
  selectedBranch: Branch;
  viewMode: boolean = false;
  headerMode = "Add";
  canUpdate: boolean = false;
  canVoid: boolean = false;
  pdfUrl: string;
  riderDetails: RiderDetails;
  riderAccounts: UserAccount[] = [];
  selectedRider: UserAccount;
  selectedDeliveryCharge: any;
  deliveryStartTime: string = environment.deliveryStartTime;
  deliveryEndTime: string = environment.deliveryEndTime;
  deliveryCharges: any[] = environment.deliveryCharges;
  editingRider=false;
  invoiceDateForDisplay: string;
  serviceChargePercentage: number;


  constructor(
    private invoiceHttpService: HttpService<Invoice>,
    private customerHttpService: HttpService<Customer>, 
    private productHttpService: HttpService<Product>,
    private branchHttpService: HttpService<Branch>,
    private genericHttpService: HttpService<any>,
    private userAccountHttpService: HttpService<UserAccount>,
    private toastrService: ToastrService, 
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private matDialog: MatDialog) {
      if("view" == activatedRoute.snapshot.url[1].path) {
        this.viewMode = true;
        let id: string = this.activatedRoute.snapshot.paramMap.get("id");
        this.invoiceHttpService.get(environment.backendApiUrl+"/invoices/invoice?id="+id+"&branch-id="+localStorage.getItem("activeBranchId")).subscribe({
          next : (httpResponse: HttpResponse<Invoice>) => {
            let activeBranchId = localStorage.getItem("activeBranchId");
            if(httpResponse.body.branchId == activeBranchId || activeBranchId == "*") {
              this.invoice = httpResponse.body;
              this.deliveryDate= new Date(Date.parse(this.invoice.deliveryDate.toString()));
              this.deliveryTime = this.deliveryDate.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
              this.discount = this.invoice.discount;
              if(this.invoice.deliveryCharge) {
                this.selectedDeliveryCharge = this.deliveryCharges
                .find(deliveryCharge => deliveryCharge.itemCode==this.invoice.deliveryCharge.itemCode);
              }
              this.computeAmountDue();
              this.getBranches();
              this.getRiderAccounts();
              this.setBranchDetailsFromBranchConfiguration(this.invoice.branchId);
              this.riderDetails = this.invoice.riderDetails;
              this.invoiceDateForDisplay = moment(this.invoice.invoiceDate).format("MMM-DD-YYYY hh:mma");
            } else {
              this.router.navigate(["invoices"]);
            }
          },
          error: error => {
            this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" });
            this.router.navigate(["invoices"]);
          }
        });
      }
    }
  
  ngOnInit() {
    this.invoice. customer = new Customer();
    this.isAdmin = '*'==localStorage.getItem('activeBranchId');
    this.canUpdate = this.checkCanUpdate();
    this.canVoid = PermissionCheckUtils.checkPermissions(Resource.INVOICES_VOID, Action.UPDATE);
    if("*"!=localStorage.getItem("activeBranchId")) {
      this.setBranchDetailsFromBranchConfiguration(localStorage.getItem("activeBranchId"));
    }
  }

  getBranches() {
    this.branchHttpService.get(environment.backendApiUrl+"/branches")
      .subscribe({
        next: (httpResponse: HttpResponse<Branch[]>) => {
          this.branches = httpResponse.body;
          this.selectedBranch = this.branches
            .find(branch => branch.branchId == this.invoice.branchId);
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  createInvoice():void {
    this.validateInvoice();
    this.sendInvoice();
  }

  editRider(): void {
    this.riderDetails
    this.editingRider=true;
  }

  saveRider(): void {
    const dialogRef = this.matDialog.open(ConfirmActionDialogComponent, {
      width: '500px', data: {id: this.invoice.id, action: "Save Rider"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.genericHttpService.put(environment.backendApiUrl+"/invoices/rider-details/"+this.invoice.id+"?branch-id="+localStorage.getItem("activeBranchId"), this.riderDetails)
        .subscribe({
          next: () => {
            this.editingRider=false;
            this.invoice.riderDetails=this.riderDetails;
          },
          error: error => {
            this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          }
        });
      }
    });
  }

  cancelRiderChanges() {
    this.editingRider=false;
    this.riderDetails=this.invoice.riderDetails;
    if(!this.riderDetails) {
      this.selectedRider = undefined;
    }
  }

  validateInvoice(): void {
    let hasMainProduct: boolean = this.invoice.items
      .filter(item => item.product.productType == ProductType.MAIN && item.quantity > 0)
      .length > 0;

    if(!hasMainProduct  && this.invoice.items.length > 0) {
      this.toastrService.error("At least 1 MAIN product must be ordered to proceed", "Invalid Invoice", {positionClass: "toast-top-center"});
      this.validInvoice = false;
      return;
    }

    let hasNeededFields = this.invoice && this.invoice.customer  && this.invoice.items.length > 0;
    let goodForDineIn: boolean = false;
    let goodForDelivery: boolean = false;
    let goodForTakeOut: boolean = false;

    let forDineIn: boolean = this.invoice.transactionType.toString() == TransactionType.DINE_IN.toString();
    if(forDineIn && this.invoice.tableNumber) {
      goodForDineIn = true;
    }
    let forDelivery: boolean = this.invoice.transactionType.toString() == TransactionType.DELIVERY.toString();
    if(forDelivery && this.invoice.customer.name && this.deliveryTime) {
      goodForDelivery = true;
    }

    let forTakeOut: boolean = this.invoice.transactionType.toString() == TransactionType.TAKE_OUT.toString();
    if(forTakeOut && this.invoice.customer.name && this.deliveryTime) {
      goodForTakeOut = true;
    }
    
    if( hasNeededFields && (goodForDineIn || goodForDelivery || goodForTakeOut) ) {     
      if(this.invoice.branchId == null) {
        this.invoice.branchId = localStorage.getItem("activeBranchId");
      }
      this.validInvoice = true;
    } else {
      this.toastrService.error("Please complete required fields", "Invalid Invoice", {positionClass: "toast-top-center"})
      this.validInvoice = false
    }
  }

  sendInvoice(): void {
    if(this.validInvoice) {
      this.invoice.amountDue = this.totalAmountDue;
      this.invoice.discount = this.discount;
      this.invoice.deliveryDate = this.deliveryDate;
      if(this.invoice.id == null){
        this.createNewInvoice();
      } else {
        this.updateInvoice();
      }
    }
  }

  private updateInvoice() {
    this.invoiceHttpService.put(environment.backendApiUrl + "/invoices", this.invoice)
      .subscribe({
        next: () => {
          this.toastrService.success("Invoice Updated", "Success", { positionClass: "toast-top-center" });
          this.router.navigate(["invoices"]);
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  private createNewInvoice() {
    this.invoice.deliveryStatus = environment.initialDeliveryStatus;
    this.invoiceHttpService.post(environment.backendApiUrl + "/invoices", this.invoice)
      .subscribe({
        next: () => {
          this.toastrService.success("Invoice Created", "Success", { positionClass: "toast-top-center" });
          this.router.navigate(["invoices"]);
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  findCustomers(customerName: string): void {
    if(customerName.length > 2){
      this.customerHttpService.get(environment.backendApiUrl+"/customers/" + customerName).subscribe({
      next: (httpResponse: HttpResponse<Customer[]>) => this.customers = httpResponse.body,
      error: (error) => this.toastrService.error(error.error, "An Error Occured", {positionClass: "toast-top-center"})
    });
    }
  }

  customerName = (customer: Customer): string => customer ? customer.name : "";

  selectCustomer(customer: Customer): void {
    this.invoice.customer = customer;
  }

  findProducts(product: string): void {
    if(product.length > 1){
      this.productHttpService.get(environment.backendApiUrl+"/products/" + product).subscribe({
      next: (httpResponse: HttpResponse<Product[]>) => this.products = httpResponse.body,
      error: (error) => this.toastrService.error(error.error, "An Error Occured", {positionClass: "toast-top-center"})
    });
    }
  }

  selectProduct(product: Product): void {
    console.log(product)
    let alreadyAdded = this.invoice.items
      .filter(item => item.product.id == product.id)
      .length > 0;

      if (alreadyAdded) {
        this.toastrService.warning("Product: "+product.name+" - "+product.productCode+",<br>is already added.<br>Please just add quantity if needed", "Invalid Action", {positionClass: "toast-top-center", enableHtml:true})
      } else {
        this.invoice.items.push({
          quantity: INITIAL_QUANTITY,
          product: product
        });
        this.computeAmountDue();
      }
  }

  productName = (product : Product): string => product.name;

  computeAmountDue(): void {
    let amountDue = 0;
    console.log(this.invoice.items);
    for(let item of this.invoice.items) {
      amountDue = amountDue + (item.quantity * item.product.price);
    }
    if(this.selectedDeliveryCharge) {
      amountDue = amountDue + this.selectedDeliveryCharge.charge
    }
    if(this.invoice.withServiceCharge) {
      this.invoice.serviceCharge = amountDue*this.serviceChargePercentage/100;
      amountDue = amountDue + this.invoice.serviceCharge;
    }
    if(this.invoice.withDiscount) {
      this.invoice.discount = amountDue*this.discount/100;
      amountDue = amountDue - this.invoice.discount;
    }
    this.totalAmountDue = amountDue;
  };

  changeDeliveryTime(time: string) {
    console.log("Delivery Time", time);
    console.log("Delivery Date" + this.deliveryDate);
    let twentyFourHourFormat: string = moment(time, ["h:mm A"]).format("HH:mm");
    console.log("Formatted to " + twentyFourHourFormat);
    let hour: number = Number(twentyFourHourFormat.split(":")[0]);
    let minute: number = Number(twentyFourHourFormat.split(":")[1]);
    this.deliveryDate.setHours(hour, minute, 0);
    console.log("New Delivery Date" + this.deliveryDate);
  }

  openCreateCustomerDialog() {
    const dialogRef = this.dialog.open(CreateCustomerDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.invoice.customer = result.customer
      }
    });
  }

  removeItem(item) {
    this.invoice.items = this.invoice.items
      .filter(itemInList => itemInList !== item);
    this.computeAmountDue();
  }

  selectNote(note): void {
    this.selectedNote = note;
  }

  addNote() {
    if(this.selectedNote != null){
      let notYetAdded: boolean = this.invoice.notes
        .filter(note => note.type == this.selectedNote.type).length == 0;
      if(notYetAdded) {
        this.invoice.notes.push(this.selectedNote);
      } else {
        this.toastrService.warning("Type Already Added", "Invalid Action", { positionClass: "toast-top-center" });
      }
    }
  }

  selectBranch(branch: Branch) {
    this.invoice.branchId = branch.branchId;
    this.setBranchDetailsFromBranchConfiguration(branch.branchId);
  }

  private setBranchDetailsFromBranchConfiguration(branchId: string) {
    this.branchHttpService.get(environment.backendApiUrl+"/branches/"+branchId).subscribe({
      next : (httpResponse: HttpResponse<Branch>) => {
        let branch: Branch = httpResponse.body;
        if(branch.branchConfiguration.numberOfTables > 0) {
          for(let i=0; i < branch.branchConfiguration.numberOfTables; i++){
            this.tables[i+1] = (i+1).toString();
          }
        } else {
          this.toastrService.warning("Please set number of tables under Branches > " + branch.name + " >  Configuration > Number of tables", "No Tables Configured", { positionClass: "toast-top-center" });
        }
        if(branch.branchConfiguration.serviceChargePercentage) {
          this.serviceChargePercentage = branch.branchConfiguration.serviceChargePercentage;
          this.computeAmountDue();
        } 
        else {
          this.serviceChargePercentage = 0;
        }
        if(branch.branchConfiguration.discountPercentage) {
          this.discount = branch.branchConfiguration.discountPercentage;
          this.computeAmountDue();
        }
        if(branch.branchConfiguration.deliveryCharges) {
          this.deliveryCharges = branch.branchConfiguration.deliveryCharges;
        }
      },
      error: error => {
        this.toastrService.error(error.error, "An Error Occured Retrieving the Branch Configuration", { positionClass: "toast-top-center" });
      }
    });
  }
  
  changeTransactionType(transactionType: TransactionType) {
    if(this.branches.length == 0) {
      this.getBranches();
    }
    if(this.riderAccounts.length == 0) {
      this.getRiderAccounts()
    }
    this.invoice.transactionType = transactionType;
     
    let hasTypedCustomerNameButNotAssigned: boolean = this.invoice.customer && !this.invoice.customer.id;
    if(hasTypedCustomerNameButNotAssigned) {
      this.invoice.customer = new Customer;
    }
  }

  getRiderAccounts() {
    this.userAccountHttpService.get(environment.iamApiUrl+"/user-accounts/riders?branch-id="+localStorage.getItem("activeBranchId")).subscribe({
      next: (httpResponse: HttpResponse<UserAccount[]>) => {
        this.riderAccounts = httpResponse.body;
        if (this.invoice.riderDetails) {
          this.selectedRider = this.riderAccounts
            .find(riderAccount => riderAccount.accountId == this.invoice.riderDetails.accountId);
        }
      },
      error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }
  
  selectRider(riderAccount: UserAccount) {
    this.riderDetails = new RiderDetails();
    this.riderDetails.accountId = riderAccount.accountId;
    this.riderDetails.personalDetails = riderAccount.personalDetails;
  }

  selectTable(table: string) {
    this.invoice.tableNumber = table;
  }

  selectDeliveryCharge(deliveryCharge: any) {
    this.invoice.deliveryCharge = deliveryCharge;
    this.computeAmountDue();
  }

  checkCanUpdate():boolean {
    return PermissionCheckUtils.checkPermissions(Resource.INVOICES, Action.UPDATE);
  }

  calculateRows(description: string): number {
    if (description) {
      return description.split('\n').length > 5 ? 6 : description.split('\n').length;
    } else {
      return 1;
    }
  }

  openConfirmDeleteDialog() {
    const dialogRef = this.matDialog.open(ConfirmActionDialogComponent, {
      width: '500px', data: {id: this.invoice.id, action: "Delete"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.invoiceHttpService.delete(environment.backendApiUrl+"/invoices/"+this.invoice.id+"?branch-id="+localStorage.getItem("activeBranchId"))
          .subscribe({
            next: () => {
              this.toastrService.success("Invoice Deleted", "Success", { positionClass: "toast-top-center" });
              this.router.navigate(["invoices"]);
            },
            error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          });
      }
    });
  }
  
  openConfirmVoidDialog() {
    const dialogRef = this.matDialog.open(ConfirmActionDialogComponent, {
      width: '500px', data: {id: this.invoice.id, action: "Void"}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.invoiceHttpService.put(environment.backendApiUrl+"/invoices/void/"+this.invoice.id+"?branch-id="+localStorage.getItem("activeBranchId"), null)
          .subscribe({
            next: () => {
              this.toastrService.success("Invoice Deleted", "Success", { positionClass: "toast-top-center" });
              this.router.navigate(["invoices"]);
            },
            error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          });
      }
    });
  }

  serviceChargeToggled(checked: boolean) {
    if(checked) {
      this.invoice.withServiceCharge = true;
    } else {
      this.invoice.withServiceCharge = false;
      this.invoice.serviceCharge = 0;
    }
    this.computeAmountDue();
  }

  discountChargeToggled(checked: boolean) {
    if(checked) {
      this.invoice.withDiscount = true;
    } else {
      this.invoice.withDiscount = false;
      this.invoice.discount = 0;
    }
    this.computeAmountDue();
  }


  @ViewChild('externalPdfViewer', {static: false}) public externalPdfViewer;
  generatePdf(reportType: string) {
    this.genericHttpService.post(
      environment.backendApiUrl + "/reports/document?report-type=" + reportType,
    {invoice:this.invoice})
      .subscribe({
        next: (httpResponse: HttpResponse<any>) => {
          this.pdfUrl = window.location.host +"/public"+ httpResponse.headers.get("Location").split("8081")[1];
          this.genericHttpService.getWithResponseType(
            httpResponse.headers.get("Location")+"?doc-type=PDF",
          'blob')
            .subscribe({
              next: (httpResponse: HttpResponse<any>) => {
                console.log(httpResponse)
                this.toastrService.success("PDF Created", "Success", { positionClass: "toast-top-center" });
                this.externalPdfViewer.pdfSrc = httpResponse;
                this.externalPdfViewer.refresh();
              },
              error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
            });
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }
}
