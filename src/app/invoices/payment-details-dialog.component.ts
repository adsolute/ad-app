import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpService } from '../shared/services/http.service';
import { Invoice, PaymentDetails, PaymentMethod } from './invoice';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-payment-details-dialog',
  templateUrl: './payment-details-dialog.component.html',
  styleUrls: ['./payment-details-dialog.component.css']
})
export class PaymentDetailsDialogComponent implements OnInit {
  paymentDetails: PaymentDetails = new PaymentDetails;
  methods: string[] = [
    PaymentMethod.CASH,
    PaymentMethod.GCASH,
    PaymentMethod.BANK_TRANSFER,
    PaymentMethod.FOOD_PANDA,
    PaymentMethod.PETTY_CASH,
    PaymentMethod.LALAMOVE,
    PaymentMethod.GRAB,
    PaymentMethod.METRO_DEAL,
    PaymentMethod.OTHER,
  ]

  constructor(
    private dialogRef: MatDialogRef<PaymentDetailsDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    private id: String,
    private httpService: HttpService<any>,
    private toastrService: ToastrService) { }

  ngOnInit() {
    console.log(this.id);
    this.httpService.get(environment.backendApiUrl+"/invoices/invoice?id="+this.id+"&branch-id="+localStorage.getItem("activeBranchId"))
      .subscribe({
        next: (httpResponse: HttpResponse<Invoice>) => {
          this.paymentDetails = httpResponse.body.paymentDetails;
          if(!this.paymentDetails) {
            this.paymentDetails = new PaymentDetails();
            this.paymentDetails.paymentMethod = PaymentMethod.CASH;
            this.paymentDetails.paymentDate = new Date();
          }
        },
        error: error => {
          this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          this.dialogRef.close();
        }
      });
  }

  savePaymentDetails() {
    this.httpService.put(environment.backendApiUrl+"/invoices/payment-details/"+this.id+"?branch-id="+localStorage.getItem("activeBranchId"), this.paymentDetails)
      .subscribe({
        next: () => this.dialogRef.close({event:"success"}),
        error: error => {
          this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          this.dialogRef.close();
        }
      });
  }
}
