import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Invoice } from './invoice';
import { HttpService } from '../shared/services/http.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-delivery-status-dialog',
  templateUrl: './delivery-status-dialog.component.html',
  styleUrls: ['./delivery-status-dialog.component.css']
})
export class DeliveryStatusDialogComponent implements OnInit {

  deliveryStatus: string;
  availableStatus: string [] = environment.availableStatuses;

  constructor(
    private dialogRef: MatDialogRef<DeliveryStatusDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    private id: String,
    private httpService: HttpService<Invoice>,
    private toastrService: ToastrService) {}

  ngOnInit() {
    console.log(this.id);
    this.httpService.get(environment.backendApiUrl+"/invoices/invoice?id="+this.id+"&branch-id="+localStorage.getItem("activeBranchId"))
      .subscribe({
        next: (httpResponse: HttpResponse<Invoice>) =>  this.deliveryStatus = httpResponse.body.deliveryStatus,
        error: error => {
          this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          this.dialogRef.close();
        }
      });
  }

  saveDeliveryStatus() {
    this.httpService.put(environment.backendApiUrl+"/invoices/"+this.id+"/delivery-status/"+this.deliveryStatus, null)
      .subscribe({
        next: () => this.dialogRef.close({event:"success"}),
        error: error => {
          this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          this.dialogRef.close();
        }
      });
  }

  
}
