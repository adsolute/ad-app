import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QueueComponent } from './queue.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [QueueComponent],
  imports: [
    CommonModule,
    MatGridListModule,
    ScrollingModule,
    SharedModule,
  ]
})
export class QueueModule { }
