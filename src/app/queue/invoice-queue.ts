import { Item } from '../invoices/invoice';

export class InvoiceQueue {
    id: string;
    invoiceNumber: string;
    customerName: string;
    items: Item[] = [];
    late:boolean;
    deliveryDateTimeString: string;
}