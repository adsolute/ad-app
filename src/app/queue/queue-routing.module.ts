import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { QueueComponent } from './queue.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: "processing_queue", component: QueueComponent }]),
    RouterModule.forChild([{ path: "pickup_queue", component: QueueComponent }]),
  ]
})
export class QueueRoutingModule { }
