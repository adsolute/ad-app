import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { HttpService } from '../shared/services/http.service';
import { InvoiceQueue } from './invoice-queue';
import { QueryParameters } from '../shared/query-parameters';
import { environment } from 'src/environments/environment';
import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { SimpleTimer } from 'ng2-simple-timer';
import { Resource } from '../roles/role';
import { TransactionType } from '../invoices/invoice';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmActionDialogComponent } from '../shared/confirm-action-dialog.component';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.css'],
})
export class QueueComponent implements OnInit {

  invoiceQueueList: InvoiceQueue[] = [];
  queryParameters: QueryParameters = new QueryParameters;
  transactionTypes: TransactionType[] = [TransactionType.DELIVERY, TransactionType.DINE_IN, TransactionType.TAKE_OUT];
  activeTransactionType: TransactionType = TransactionType.DELIVERY;
  queueUrl: string;
  status;

  constructor(
    private httpService: HttpService<InvoiceQueue>,
    private toastrService: ToastrService,
    private simpleTimer: SimpleTimer,
    private matDialog: MatDialog) {
      simpleTimer.delTimer("updateQueue");
      simpleTimer.newTimer("updateQueue", 5);
    }

  ngOnInit() {
    let urlParameters: string[] = location.pathname.split("/");
    this.queueUrl = urlParameters[urlParameters.length - 1].toUpperCase();
    this.getInvoiceQueue();
    this.simpleTimer.subscribe("updateQueue", () => {
      if(this.queueUrl == Resource.PROCESSING_QUEUE.toUpperCase()
        || this.queueUrl == Resource.PICKUP_QUEUE.toUpperCase()) {
        this.getInvoiceQueue();
      }
    });
  }

  ngOnDestroy() {
    console.log("Destroying timer for updateQueue");
    this.simpleTimer.delTimer("updateQueue");
  }

  getInvoiceQueue() {
    if(this.queueUrl == Resource.PROCESSING_QUEUE.toUpperCase()) {
      this.status = "PROCESSING";
    }
    if(this.queueUrl == Resource.PICKUP_QUEUE.toUpperCase()) {
      this.status = "FOR_PICK_UP";
    }
    this.queryParameters.criteria["deliveryStatus"]=this.status;
    this.queryParameters.criteria["transactionType"]=this.activeTransactionType;
    this.queryParameters.criteria["voided"]=false;
    this.queryParameters.dateRanges = [];

    if(this.status == "PROCESSING") {
      let startDateToday: Date = new Date();
      let endDateToday: Date = new Date();
      startDateToday.setHours(0,0,0,0);
      endDateToday.setHours(23,59,59,999);
      this.queryParameters.dateRanges = [{
        field: "deliveryDate",
        start: startDateToday,
        end: endDateToday
      }];
    }

    this.httpService.postQueryWithNoOverlay<QueryParameters, InvoiceQueue[]>(environment.backendApiUrl+"/invoices/query/queue?branch-id=" + localStorage.getItem("activeBranchId"), this.queryParameters)
    .subscribe({
      next: (httpResponse: HttpResponse<InvoiceQueue[]>) => {
        this.invoiceQueueList = httpResponse.body;
      },
      error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }

  changeTransactionType() {
    this.getInvoiceQueue();
  }

  openConfirmDialog(queue: InvoiceQueue) {
    let actionToConfirm: string;
    if(this.queueUrl == Resource.PROCESSING_QUEUE.toUpperCase()) {
      actionToConfirm = "Finish Preparing";
    }
    if(this.queueUrl == Resource.PICKUP_QUEUE.toUpperCase()) {
      actionToConfirm = "Finish Pickup";
    }
    const dialogRef = this.matDialog.open(ConfirmActionDialogComponent, {
      width: '500px', data: {id: queue.id, action: actionToConfirm}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        let nextStatus: string;
        if(this.queueUrl == Resource.PROCESSING_QUEUE.toUpperCase()) {
          nextStatus = "FOR_DELIVERY";
        }
        if(this.queueUrl == Resource.PICKUP_QUEUE.toUpperCase()) {
          nextStatus = "DONE";
        }
        this.httpService.put(environment.backendApiUrl+"/invoices/queue/"+queue.id+"?delivery-status="+nextStatus, null)
        .subscribe({
          next: () => {
            dialogRef.close({event:"success"});
            this.getInvoiceQueue();
          },
          error: error => {
            this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
            dialogRef.close();
          }
        });
      }
    });
  }

  colorBaseOnDelay(isLate: boolean): string {
    if(isLate) {
      return "#FFCCCB";
    } else {
      return "#D3D3D3";
    }
  }

  colorBaseBySameDayDelivery(forSameDayDelivery: boolean) {
    if(forSameDayDelivery) {
      return "RED";
    } else {
      return "#D3D3D3";
    }
  }

}
