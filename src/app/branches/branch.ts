import { Address } from '../shared/address';
import { ContactInformation } from '../shared/contact-information';

export class Branch {
    id: string;
    name: string;
    branchId: string;
    address: Address;
    contactInformation: ContactInformation;
    branchConfiguration: BranchConfiguration;
}

export class BranchConfiguration {
    numberOfTables: number;
    canViewUniversalProducts: boolean;
    hasOwnProducts: boolean;
    serviceChargePercentage: number;
    discountPercentage: number;
    deliveryCharges: DeliveryCharge[] = [];
}

export class DeliveryCharge {
    distance: string;
    itemCode: string;
    incentive: number;
    minimumOrder: string;
    charge: number;
}