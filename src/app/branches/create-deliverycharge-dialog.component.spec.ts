import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateDeliverychargeDialogComponent } from './create-deliverycharge-dialog.component';

describe('CreateDeliverychargeDialogComponent', () => {
  let component: CreateDeliverychargeDialogComponent;
  let fixture: ComponentFixture<CreateDeliverychargeDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateDeliverychargeDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDeliverychargeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
