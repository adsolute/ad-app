import { Component, OnInit } from '@angular/core';
import { ContactInformation } from '../shared/contact-information';
import { Address } from '../shared/address';
import { Branch, BranchConfiguration } from './branch';
import { HttpService } from '../shared/services/http.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpResponse } from '@angular/common/http';
import { CanUpdateChecker } from '../interfaces/can-update-checker';
import { PermissionCheckUtils } from '../shared/permission-check-utils';
import { Resource, Action } from '../roles/role';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmActionDialogComponent } from '../shared/confirm-action-dialog.component';
import { CreateDeliverychargeDialogComponent } from './create-deliverycharge-dialog.component';

@Component({
  selector: 'app-create-branch',
  templateUrl: './create-branch.component.html',
  styleUrls: ['./create-branch.component.css']
})
export class CreateBranchComponent implements OnInit, CanUpdateChecker {

  branch: Branch = new Branch;
  contactInformation: ContactInformation = new ContactInformation;
  branchConfiguration: BranchConfiguration = new BranchConfiguration;
  address: Address = new Address;
  headerMode: string = "Add"
  viewMode: boolean = false;
  canUpdate: boolean = false;

  constructor(
    private httpService: HttpService<Branch>,
    private toastrService: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private matDialog: MatDialog) {
    if ("view" == activatedRoute.snapshot.url[1].path) {
      this.viewMode = true;
      let id: string = this.activatedRoute.snapshot.paramMap.get("id");
      this.httpService.get(environment.backendApiUrl + "/branches/branch?id=" + id + "&branch-id=" + localStorage.getItem("activeBranchId")).subscribe({
        next: (httpResponse: HttpResponse<Branch>) => {
          this.address = httpResponse.body.address;
          this.contactInformation = httpResponse.body.contactInformation;
          this.branchConfiguration = httpResponse.body.branchConfiguration;
          this.branch = httpResponse.body;
        },
        error: error => {
          this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" });
          this.router.navigate(["branches"]);
        }
      });
    }
  }

  ngOnInit() {
    this.canUpdate = this.checkCanUpdate();
  }

  saveBranch() {
    this.branch.address = this.address;
    this.branch.contactInformation = this.contactInformation;
    this.branch.branchConfiguration = this.branchConfiguration;
    if (this.branch.id) {
      this.updateBranch();
    } else {
      this.createBranch();
    }
  }

  createBranch() {
    this.httpService.post(environment.backendApiUrl + "/branches?branch-id=" + localStorage.getItem("activeBranchId"), this.branch).subscribe({
      next: () => {
        this.toastrService.success("Branch Created", "Success", { positionClass: "toast-top-center" });
        this.router.navigate(["branches"]);
      },
      error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }

  updateBranch() {
    this.httpService.put(environment.backendApiUrl + "/branches?branch-id=" + localStorage.getItem("activeBranchId"), this.branch).subscribe({
      next: () => {
        this.toastrService.success("Branch Updated", "Success", { positionClass: "toast-top-center" });
        this.router.navigate(["branches"]);
      },
      error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
    });
  }

  checkCanUpdate(): boolean {
    return PermissionCheckUtils.checkPermissions(Resource.BRANCHES, Action.UPDATE);
  }

  openConfirmDeleteDialog() {
    const dialogRef = this.matDialog.open(ConfirmActionDialogComponent, {
      width: '500px', data: { id: this.branch.id, action: "Delete" }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.httpService.delete(environment.backendApiUrl + "/branches/" + this.branch.id + "?branch-id=" + localStorage.getItem("activeBranchId"))
          .subscribe({
            next: () => {
              this.toastrService.success("Branch Deleted", "Success", { positionClass: "toast-top-center" });
              this.router.navigate(["branches"]);
            },
            error: (error) => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          });
      }
    });
  }

  openCreateDeliveryChargeDialog() {
    const dialogRef = this.dialog.open(CreateDeliverychargeDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if(result) {
        this.branchConfiguration.deliveryCharges.push(result.deliveryCharge)
      }
    });


  }



}
