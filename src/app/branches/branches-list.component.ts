import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { PaginatedList } from '../interfaces/paginated-list';
import { Resource } from '../roles/role';
import { PagedList } from '../shared/paged-list';
import { PagedResultSet } from '../shared/paged-result-set';
import { Header, Filter, DateRangeFilter } from '../shared/paginated-table.component';
import { QueryParameters } from '../shared/query-parameters';
import { HttpService } from '../shared/services/http.service';
import { Branch } from './branch';

@Component({
  selector: 'app-branches-list',
  templateUrl: './branches-list.component.html',
  styleUrls: ['./branches-list.component.css']
})
export class BranchesListComponent extends PagedList implements OnInit, PaginatedList {

  branchPagedResultSet: PagedResultSet<Branch> = new PagedResultSet;
  queryParameters: QueryParameters = new QueryParameters;

  constructor(
    private httpService: HttpService<Branch>,
    private toastrService: ToastrService,
    private router: Router) { 
      super();
    }

  getHeaders(): Header[] {
    return [
      {display: "name", actual: "name", sortable: true},
      {display: "branchId", actual: "branchId", sortable: true},
      {display: "address", actual: "address", sortable: true},
      {display: "email", actual: "email", sortable: true},
      {display: "phone", actual: "phone", sortable: true},
      {display: "createDate", actual: "createDate", sortable: true, isADateField: true},
    ];
  }

  filters: Filter[] = [
    { name: "Name", key: "name"},
    { name: "Address", key: "address.city"},
    { name: "Email", key: "contactInformation.emailAddress"},
    { name: "Phone", key: "contactInformation.phoneNumber"},
  ];
  
  dateRanges: DateRangeFilter[] = [
    { name: "Create Date", key: "createDate"},
    { name: "Update Date", key: "updateDate"},
  ];

  ngOnInit() {
    this.queryParameters.page = 1;
    this.queryParameters.limit = 50;
    this.sendQueryParameters();
  }

  getPages = () => super.getPages(this.branchPagedResultSet);
  
  sendQueryParameters() {
    this.httpService.postQuery<QueryParameters, PagedResultSet<Branch>>(environment.backendApiUrl+"/branches/query?branch-id=" + localStorage.getItem("activeBranchId"), this.queryParameters)
      .subscribe({
        next: (httpResponse: HttpResponse<PagedResultSet<Branch>>) => this.branchPagedResultSet = httpResponse.body,
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  goToCreateBranchLink(): void {
    this.router.navigate(["branches/create"]);
  }

  hideCreateButton = () => super.hideCreateButton(Resource.BRANCHES);
}
