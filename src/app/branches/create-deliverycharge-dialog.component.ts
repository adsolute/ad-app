import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { BranchConfiguration, DeliveryCharge } from './branch';
import { CreateBranchComponent } from './create-branch.component'; //added


@Component({
  selector: 'app-create-deliverycharge-dialog',
  templateUrl: './create-deliverycharge-dialog.component.html',
  styleUrls: ['./create-deliverycharge-dialog.component.css']
})
export class CreateDeliverychargeDialogComponent implements OnInit {

  deliveryCharge: DeliveryCharge = new DeliveryCharge();

  constructor(
    private dialogRef: MatDialogRef<CreateDeliverychargeDialogComponent>) { }

  ngOnInit(): void {
  }

  addDeliveryCharge() {
    console.log(this.deliveryCharge);
    this.dialogRef.close({event:"closed", deliveryCharge:this.deliveryCharge});
  }

}
