import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BranchesListComponent } from './branches-list.component';
import { CreateBranchComponent } from './create-branch.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: "branches", component: BranchesListComponent },
      { path: "branches/create", component: CreateBranchComponent },
      { path: "branches/view/:id", component: CreateBranchComponent },
    ]),
  ]
})
export class BranchesRoutingModule { }
