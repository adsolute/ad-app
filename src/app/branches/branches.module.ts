import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BranchesListComponent } from './branches-list.component';
import { SharedModule } from '../shared/shared.module';
import { CreateBranchComponent } from './create-branch.component';
import { CreateDeliverychargeDialogComponent } from './create-deliverycharge-dialog.component';



@NgModule({
  declarations: [BranchesListComponent, CreateBranchComponent, CreateDeliverychargeDialogComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class BranchesModule { }
