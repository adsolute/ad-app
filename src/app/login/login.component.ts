import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpService } from '../shared/services/http.service';
import { HttpResponse } from '@angular/common/http';
import { UserAccount } from '../shared/user-account';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  title: string = environment.brandName;
  username: string;
  password: string;
  @Output()
  loginClickedEvent: EventEmitter<string> = new EventEmitter();

  constructor(
    private toastrService: ToastrService,
    private httpService: HttpService<Object>) { }

  login(): void {
    var credentials = {
      username : this.username,
      password : this.password
    }
    this.httpService.post(environment.iamApiUrl+"/auth/login", credentials).subscribe({
      next: (httpResponse: HttpResponse<UserAccount>) => {
        let userAccount = httpResponse.body;
          if(userAccount.grantedRoles.length > 0) {
            localStorage.setItem("activeBranchId", userAccount.grantedRoles[0].onAccountId);
          } else if(+userAccount.accountId == 1) {
            localStorage.setItem("activeBranchId", "*");
          }
        sessionStorage.setItem("isLoggedIn", "true");
        console.log(httpResponse)
      },
      error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" }),
      complete: () => this.loginClickedEvent.emit()
    });
  }

  forgotPassword(): void {
    this.toastrService.warning("Not Yet Implemented, Please Contact the Admin");
  }

  ngOnInit() {}

}
