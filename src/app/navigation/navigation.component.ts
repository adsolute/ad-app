import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, from } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MatSidenav } from '@angular/material/sidenav';
import { Branch, BranchConfiguration } from '../branches/branch';
import { HttpService } from '../shared/services/http.service';
import { HttpResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UserAccount } from '../shared/user-account';
import { Role, Action, Permission } from '../roles/role';
import { GrantedRole } from '../roles/granted-role';
import { environment } from 'src/environments/environment';
import { version } from 'package.json';
import { Address } from '../shared/address';
import { ContactInformation } from '../shared/contact-information';
import { NavigationService } from '../shared/services/navigation.service';

const ACTIVE_BRANCH_ID = "activeBranchId";
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  
  title: string = environment.brandName;
  userAccount: UserAccount;
  associatedBranches: Branch[] = [];
  activeBranch: Branch;
  readPermissions: Permission[] = [];
  version: string = version;

  @Output()
  logoutClickedEvent: EventEmitter<string> = new EventEmitter();
  @ViewChild("sideDrawer", {static : false}) drawer: MatSidenav
  inHandsetMode: boolean;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private userAccountHttpService: HttpService<UserAccount>,
    private httpService: HttpService<any>,
    private roleHttpService: HttpService<Role>,
    private toastrService: ToastrService,
    private router: Router,
    private navigationService: NavigationService) {
      this.userAccountHttpService.get(environment.iamApiUrl+"/user-accounts")
        .subscribe({
          next: (httpResponse: HttpResponse<UserAccount>) => {
            this.userAccount = httpResponse.body;
            if(this.userAccount.grantedRoles.length > 0) {
              let role = this.determineActiveRole();
              this.getActiveRole(role.roleId);
              this.getAssociatedBranches();
            } else if(+this.userAccount.accountId == 1) {
              this.getBranches();
              this.storeAdminPermissions();
            }
          },
          error: error => {
            if(error.status != 0) {
              this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
            }
          }
        });
  }

  private determineActiveRole(): GrantedRole {
    let role: GrantedRole;
    if (localStorage.getItem(ACTIVE_BRANCH_ID)) {
      role = this.userAccount.grantedRoles
        .find(grantedRole => localStorage.getItem(ACTIVE_BRANCH_ID) == grantedRole.onAccountId);
    }

    return role != null ? role : this.userAccount.grantedRoles[0];
  }

  private storeAdminPermissions() {
    localStorage.setItem("activeBranchId", "*");
    let permissions = this.userAccount.grantedPermissions.map(grantedPermission => grantedPermission.permission);
    sessionStorage.setItem("permissions", JSON.stringify(permissions));
    this.navigationService.changeActiveBranch("*");
  }

  getActiveRole(roleId: string) {
    this.roleHttpService.get(environment.iamApiUrl+"/roles/user/role?role-id="+roleId)
      .subscribe({
        next: (httpResponse: HttpResponse<Role>) => {
          let role = httpResponse.body;
          this.readPermissions = role.permissions.filter(permission => permission.action.toUpperCase() === Action.READ.toUpperCase());
          let viewPermitted: boolean = this.readPermissions
            .filter(permission => permission.resource.toUpperCase() == location.pathname.split("/")[1].toUpperCase())
            .length > 0;
          if(!viewPermitted) {
            this.router.navigate(["home"])
          }
          sessionStorage.setItem("permissions", JSON.stringify(role.permissions));
        },
        error: error => {
          this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
          this.router.navigate(["home"])
          this.readPermissions = [];
          sessionStorage.removeItem("permissions");
        }
      });
  }

  getAssociatedBranches() {
    let branchIds: string[] = [];
    this.userAccount.grantedRoles.forEach(grantedRole => branchIds.push(grantedRole.onAccountId));
    this.httpService.post(environment.backendApiUrl+"/branches/list?by-field=branchId", branchIds)
      .subscribe({
        next: (httpResponse: HttpResponse<Branch[]>) => {
          this.associatedBranches = httpResponse.body;
          if(this.associatedBranches.length > 0) {
            if(localStorage.getItem(ACTIVE_BRANCH_ID)){
              this.activeBranch = this.associatedBranches
                .find(associatedBranch => localStorage.getItem(ACTIVE_BRANCH_ID) == associatedBranch.branchId);
            } else {
              this.activeBranch = this.associatedBranches[0];
              localStorage.setItem(ACTIVE_BRANCH_ID, this.activeBranch.branchId);
            }
          }
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  getBranches() {
    let allBranch: Branch = {
      id: "1",
      name: "All",
      branchId: "*",
      address: new Address,
      contactInformation: new ContactInformation,
      branchConfiguration: new BranchConfiguration
    };
    this.httpService.get(environment.backendApiUrl+"/branches")
      .subscribe({
        next: (httpResponse: HttpResponse<Branch[]>) => {
          this.associatedBranches = httpResponse.body;
          this.associatedBranches.unshift(allBranch);
          this.activeBranch = this.associatedBranches[0];
          localStorage.setItem(ACTIVE_BRANCH_ID, this.activeBranch.branchId);
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
  }

  logout(): void {
    sessionStorage.removeItem("isLoggedIn");
    console.log("Sending Logout event");
    sessionStorage.clear();
    localStorage.clear();
    this.httpService
      .delete(environment.iamApiUrl+"/auth/logout")
      .subscribe({
        next: () => {
          this.toastrService.info("Session Ended", "Logged Out", { positionClass: "toast-top-center" })
        },
        error: error => this.toastrService.error(error.error, "An Error Occured", { positionClass: "toast-top-center" })
      });
    this.logoutClickedEvent.emit();
  }

  closeDrawerInOverMode() {
    if (this.drawer.mode === 'over'){
      this.drawer.close();
    }
  }

  changeActiveBranch(activeBranch){
    localStorage.setItem("activeBranchId", this.activeBranch.branchId);
    if(+this.userAccount.accountId != 1) {
      let grantedRole: GrantedRole = this.userAccount.grantedRoles
        .find(grantedRole => grantedRole.onAccountId == activeBranch.branchId);
      this.getActiveRole(grantedRole.roleId);
    }
    this.closeDrawerInOverMode();
    this.navigationService.changeActiveBranch(activeBranch.branchId);
    this.router.navigate(["home"]);
  }

  hideNavigationItem(item: string): boolean {
    if(this.userAccount != null) {
      let hasNoReadPermissionOnItem = this.readPermissions.filter(permission => permission.resource == item.toUpperCase() || (item=='audits') && permission.resource.includes('AUDIT')).length == 0;
      let isNotRootAdmin = +this.userAccount.accountId != 1;
      return hasNoReadPermissionOnItem && isNotRootAdmin;
    }
    return true;
  }

  goToChangeLogs(){
    window.open(location.origin+"/changelog");
  }
}
