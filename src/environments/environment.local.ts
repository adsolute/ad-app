// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  backendApiUrl: "http://localhost:8081",
  iamApiUrl: "http://localhost:8082",
  auditApiUrl: "http://localhost:8083",
  brandName: "Samgyup sa Bahay",
  availableStatuses: ["PROCESSING", "FOR_DELIVERY", "DELIVERED", "FOR_PICK_UP", "DONE"],
  initialDeliveryStatus: "PROCESSING",
  processingStatus: "FOR_DELIVERY",
  deliveredStatus: "DELIVERED",
  forPickUpStatus: "FOR_PICK_UP",
  doneStatus: "DONE",
  deliveryStartTime: "9:00",
  deliveryEndTime: "22:00",
  deliveryCharges: [
    {distance:"0-10km", itemCode: "FREE", incentive:0, minimumOrder: "NO MINIMUM", charge: 0},
    {distance:"11-15km", itemCode: "DC1", incentive:15, minimumOrder: "3PAX", charge: 70},
    {distance:"16-20km", itemCode: "DC2", incentive:20, minimumOrder: "4PAX", charge: 100},
    {distance:"21-25km", itemCode: "DC3", incentive:30, minimumOrder: "6PAX", charge: 150},
    {distance:"26-30km", itemCode: "DC4", incentive:40, minimumOrder: "6PAX", charge: 200},
    {distance:"31-35km", itemCode: "DC5", incentive:50, minimumOrder: "8PAX", charge: 250},
    {distance:"36-40km", itemCode: "DC6", incentive:60, minimumOrder: "8PAX", charge: 300},
  ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
