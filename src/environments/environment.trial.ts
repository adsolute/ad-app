export const environment = {
  production: true,
  backendApiUrl: "http://3.19.69.136:8081",
  iamApiUrl: "http://3.19.69.136:8082",
  brandName: "Gourment to Go MNL",
  availableStatuses: ["PROCESSING", "FOR_DELIVERY", "DELIVERED", "FOR_PICK_UP"],
  initialDeliveryStatus: "PROCESSING",
  processingStatus: "FOR_DELIVERY",
  deliveredStatus: "DELIVERED",
  forPickUpStatus: "FOR_PICK_UP",
  doneStatus: "DONE",
  deliveryStartTime: "9:00",
  deliveryEndTime: "22:00",
  deliveryCharges: [
    {distance:"0-10km", itemCode: "FREE", incentive:0, minimumOrder: "NO MINIMUM", charge: 0},
    {distance:"11-15km", itemCode: "DC1", incentive:15, minimumOrder: "3PAX", charge: 70},
    {distance:"16-20km", itemCode: "DC2", incentive:20, minimumOrder: "4PAX", charge: 100},
    {distance:"21-25km", itemCode: "DC3", incentive:30, minimumOrder: "6PAX", charge: 150},
    {distance:"26-30km", itemCode: "DC4", incentive:40, minimumOrder: "6PAX", charge: 200},
    {distance:"31-35km", itemCode: "DC5", incentive:50, minimumOrder: "8PAX", charge: 250},
    {distance:"36-40km", itemCode: "DC6", incentive:60, minimumOrder: "8PAX", charge: 300},
  ]
};
