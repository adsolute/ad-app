# Change Log

## 0.9.0
### Added
- Added new module - Audit
- Added viewing of the Audit history

### Changed
- Updated Products to include number of Pax field
- Updated Invoice - Dine in to indicate number of pax in the table
- Included Number of Pax in daily sales report
- Included Original Amount and Discount field in daily sales report
- Moved the breakdown of balances in another page/sheet
- Included the breakdown of ordered products on the second page alongside with the balance breakdown

## 0.8.2
### Added
- Added @angular/localize to fix the bug in Schedule caused by Angular9 onwards update

## 0.8.1
### Added
- Added Service Charge in Invoice as requested by the business
- Removed some invoice from June 1 to June 23, 2021 as per business request

## 0.8.0
### Added
- Daily Sales Report document to show the sales in an excel file
- Dashboard showing the details of all the invoices for today's delivery
- Payment Details in an invoice
- Branch configuration to make the table number dynamic per branch
- Receipt Generation

## 0.7.1
### Added
- Delivery time validation on the back end before saving the invoice
- Added Invoice number on display of invoice details.

### Changed
- Fixed Queue tab's timezone issue
- Fixed Delivery time issue

## 0.7.0
### Added
- Added new Module, QUEUE.
- Bug fix for creating Invoice when adding an empty Notes
- Added FOR RIDER switch no roles
- Delete functionality in Accounts
- Delete functionality in Customers
- Delete functionality in Branches
- Delete functionality in Invoice
- Added Rider Details field on Invoice

### Changed
- Updated Delivery Status color scheme.
- Updated default limit per page to 50 records per table
- Added Total Sales Report and added a graph to show the time that orders are usually placed.
- Changed the session timeout from default 30 minutes to  6 hours.

## 0.6.0
### Added
- Invoice PDF Genaration and viewing
- Search filter for the paginated tables.
- Date Range filter of some paginated tables where date range may be applied.
- Sorting of paginated table fields (some fields are unsortable).
- Viewing/Redirecting from schedule page to invoice page when the schedule was double click.

### Changed
- Bug fix for Invoice that has transaction type of Dine in and the delivery status fails.
- Bug fix for Account editing.
- Bug fix for Product Deletion.

## 0.5.0
### Added
- View and edit mode of Branches.
- Displayed product code and name on product search in invoice creation.
- Added Delete button, but not all delete button works yet.
- Product deletion with confirmation dialog box.
- View and edit mode of Roles.
- View and edit mode of Accounts. (Change password to be implemented soon)

### Changed
- Made the create customer dialog from invoice creation, to a responsive dialog so that it can be viewed on mobile devices
- Updated template of edit button

# Change Log

## 0.4.0
### Added
- Delivery Status Modal from the invoice list paged.
- Delivery Status permission on the Role creation page.
- Customer Creation Modal dialog from the invoice creation page.

### Changed
- Instead of redirecting to Customer creation from Invoice creation page when creating a new customer. The new Customer creation dialog will pop up instead.
- Fixed a found bug in the table list, wherein if in the final pages, additional pages are being shown even there is no next pages truly exists.

## 0.3.1
### Added

### Changed
- Computes total amount due when a product is added to an invoice.
- Changed invoice status to delivery status. Available status are: For Delivery, Delivered, For Pick Up.
- Schedule now displays customer last name only

## 0.3.0
### Added
- Added timer in Schedule page to update the data displayed every 15 secs
- Added Default sorting of invoice to show newest invoice first
- Added an Incremental Invoice Number that is more readable than the randomly generated Invoice Id
- Added Product code and Product type when creating a Product
- Product Update functionality
- Displayed branch id in the Branches table
- Displayed current Front end build version in the navigations lower left.
- Added this CHANGELOG.md that will be displayed when build version is double clicked

### Changed
- Fixed Add Customer button during invoice creation not showing.
- Adjusted scheduler height as requested by the users. Defaulted display to business hours.
- Updated the display of events in the scheduler, to display name city and product code instead of invoice id
- Added validation on Invoice creation. Invoice must have at least 1 MAIN product type to proceed.